﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.Data.DomainModel;
using JetBrains.Annotations;
using Mehdime.Entity;

namespace Core.Data.Repository
{
    public class GenericRepository<T> : IRepository<T> where T : class, IDomainObject
    {
        private readonly IAmbientDbContextLocator _ambientDbContextLocator;

        public GenericRepository([NotNull] IAmbientDbContextLocator ambientDbContextLocator)
        {
            _ambientDbContextLocator = ambientDbContextLocator;
        }

        [NotNull]
        private MainDbContext DbContext
        {
            get
            {
                var dbContext = _ambientDbContextLocator.Get<MainDbContext>();

                if (dbContext == null)
                    throw new InvalidOperationException("No ambient DbContext of type MainDbContext found. " +
                        "The repository method has been called outside of the DbContextScope scope.");

                return dbContext;
            }
        }
        public void Import(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.Id = Get().Where(x => x.Identity == entity.Identity).Select(y => y.Id).FirstOrDefault();
                Update(entity);
            }
        }

        public void Import(T entity)
        {
            entity.Id = Get().Where(x => x.Identity == entity.Identity).Select(y => y.Id).FirstOrDefault(); ;
            Update(entity);
        }

        [NotNull]
        protected virtual DbSet<T> DbSet => DbContext.Set<T>();

        public virtual T GetById(int id) => DbSet.Find(id);

        public virtual void Clear() => DbSet.RemoveRange(DbSet);

        public virtual Task<T> GetByIdAsync(int id) => DbSet.FindAsync(id);

        public virtual IQueryable<T> Get() => DbSet;

        public virtual void Add(T entity) => DbSet.Add(entity);

        public virtual void Update(T entity)
        {
            if (entity.Id == 0)
            {
                Add(entity);
                return;
            }

            if (DbContext.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            if (DbContext.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            DbSet.Remove(entity);
        }
    }
}
