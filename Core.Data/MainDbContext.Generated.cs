using System.Data.Entity;
using Core.Data.DomainModel;

namespace Core.Data
{
    public partial class MainDbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Config> Configs { get; set; }
    }
}
