using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Core.Data.DomainModel
{
    public partial class Role
    {
        public Role()
        {
        }
    }


    public partial class User
    {
        public User()
        {
        }
    }


    public partial class Employee : IDomainObject
    {
        public Employee()
        {
        }
        [Key]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Required]
        public string Name { get; set; }

        public string Img { get; set; }
        [NotMapped]
        public KaptlMedia ImgMedia
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Img))
                {
                    return null;
                }
                var mediaList = JsonConvert.DeserializeObject<List<KaptlMedia>>(Img);
                if (mediaList == null)
                {
                    return null;
                }
                return mediaList.FirstOrDefault();
            }
            set
            {
                Img = value == null ? "" : JsonConvert.SerializeObject(new List<KaptlMedia>() { value }, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            }
        }

        public string EmployeeFile { get; set; }
        [NotMapped]
        public KaptlMedia EmployeeFileMedia
        {
            get
            {
                if (string.IsNullOrWhiteSpace(EmployeeFile))
                {
                    return null;
                }
                var mediaList = JsonConvert.DeserializeObject<List<KaptlMedia>>(EmployeeFile);
                if (mediaList == null)
                {
                    return null;
                }
                return mediaList.FirstOrDefault();
            }
            set
            {
                EmployeeFile = value == null ? "" : JsonConvert.SerializeObject(new List<KaptlMedia>() { value }, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            }
        }

        [Required]
        public string EmployeeGallery { get; set; }
        [NotMapped]
        public IEnumerable<KaptlMedia> EmployeeGalleryMediaList
        {
            get
            {
                if (string.IsNullOrWhiteSpace(EmployeeGallery))
                {
                    return new List<KaptlMedia>();
                }
                return JsonConvert.DeserializeObject<List<KaptlMedia>>(EmployeeGallery);
            }
            set
            {
                EmployeeGallery = value == null ? "" : JsonConvert.SerializeObject(value);
            }
        }

        [Required]
        public string EmployeeFileList { get; set; }
        [NotMapped]
        public IEnumerable<KaptlMedia> EmployeeFileListMediaList
        {
            get
            {
                if (string.IsNullOrWhiteSpace(EmployeeFileList))
                {
                    return new List<KaptlMedia>();
                }
                return JsonConvert.DeserializeObject<List<KaptlMedia>>(EmployeeFileList);
            }
            set
            {
                EmployeeFileList = value == null ? "" : JsonConvert.SerializeObject(value);
            }
        }

        // n -> n
        [InverseProperty("Employees")]
        public virtual ICollection<Project> Projects { get; set; }
    }


    public partial class Project : IDomainObject
    {
        public Project()
        {
        }
        [Key]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        // n -> n
        [InverseProperty("Projects")]
        [DefaultValue("")]
        public virtual ICollection<Employee> Employees { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public string ProjectGallery { get; set; }
        [NotMapped]
        public IEnumerable<KaptlMedia> ProjectGalleryMediaList
        {
            get
            {
                if (string.IsNullOrWhiteSpace(ProjectGallery))
                {
                    return new List<KaptlMedia>();
                }
                return JsonConvert.DeserializeObject<List<KaptlMedia>>(ProjectGallery);
            }
            set
            {
                ProjectGallery = value == null ? "" : JsonConvert.SerializeObject(value);
            }
        }
    }


    public partial class Config : IDomainObject
    {
        public Config()
        {
        }
        [Key]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Required]
        public string Key { get; set; }

        [Required]
        public string Value { get; set; }
    }


}
