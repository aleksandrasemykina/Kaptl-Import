﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Data.Entity.Migrations;
using System.Linq;
using Core.Data.DomainModel;
using Microsoft.AspNet.Identity;

namespace Core.Data
{
    public class MainDbMigrationConfiguration : DbMigrationsConfiguration<MainDbContext>
    {
        public MainDbMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(MainDbContext context)
        {
            var applicationRole = new Role
            {
                Name = "admin"
            };

            if (context.Roles.All(role => role.Name != applicationRole.Name))
            {
                context.Roles.Add(applicationRole);
                context.SaveChanges();
            }

            var passwordHasher = new PasswordHasher();

            var applicationUser = new User
            {
                UserName = "admin",
                Email = "admin@localhost",
                SecurityStamp = "e1b74d66-42ed-440e-abc1-fe469bd86b39",
                PasswordHash = passwordHasher.HashPassword("admin"),
                EmailConfirmed = true
            };

            if (!context.Users.All(user => user.UserName != applicationUser.UserName))
                return;

            context.Users.Add(applicationUser);
            context.SaveChanges();

            applicationUser.Roles.Add(new UserRole
            {
                RoleId = applicationRole.Id,
                UserId = applicationUser.Id
            });

            context.SaveChanges();
        }
    }
}
