using Mehdime.Entity;
using Core.Data.DomainModel;
using Core.Data.Repository;

namespace Core.DataAccess
{
    public partial interface IRoleRepository : IRepository<Role>
    {
    }

    public partial class RoleRepository : GenericRepository<Role>, IRoleRepository
    {
        public RoleRepository(IAmbientDbContextLocator ambientDbContextLocator) : base(ambientDbContextLocator)
        {
        }
    }

    public partial interface IUserRepository : IRepository<User>
    {
    }

    public partial class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(IAmbientDbContextLocator ambientDbContextLocator) : base(ambientDbContextLocator)
        {
        }
    }

    public partial interface IEmployeeRepository : IRepository<Employee>
    {
    }

    public partial class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(IAmbientDbContextLocator ambientDbContextLocator) : base(ambientDbContextLocator)
        {
        }
    }

    public partial interface IProjectRepository : IRepository<Project>
    {
    }

    public partial class ProjectRepository : GenericRepository<Project>, IProjectRepository
    {
        public ProjectRepository(IAmbientDbContextLocator ambientDbContextLocator) : base(ambientDbContextLocator)
        {
        }
    }

    public partial interface IConfigRepository : IRepository<Config>
    {
    }

    public partial class ConfigRepository : GenericRepository<Config>, IConfigRepository
    {
        public ConfigRepository(IAmbientDbContextLocator ambientDbContextLocator) : base(ambientDbContextLocator)
        {
        }
    }

}
