/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Specialized;
using JetBrains.Annotations;

namespace Core.Services
{
    public class ConfigService : IConfigService
    {
        private readonly NameValueCollection _collection;

        public ConfigService([NotNull] NameValueCollection collection)
        {
            _collection = collection;

            MailFrom = GetConfigSettingString("MailFrom");
            BccTo = GetConfigSettingString("BccTo");
            SmtpServer = GetConfigSettingString("SmtpServer");
            SmtpLogin = GetConfigSettingString("SmtpLogin");
            SmtpPassword = GetConfigSettingString("SmtpPassword");
            SmtpPort = GetConfigSettingInt("SmtpPort");
            SendNotificationsTo = GetConfigSettingString("SendNotificationsTo");
            UploadFolder = GetConfigSettingString("UploadFolder");            
            ResizedImagesFolder = GetConfigSettingString("ResizedImagesFolder");           
            UploadMaxWidth = GetConfigSettingInt("UploadMaxWidth", 2000);
            UploadMaxHeight = GetConfigSettingInt("UploadMaxHeight", 2000);
            UploadQuality = GetConfigSettingInt("UploadQuality", 89);
            ItemsPerPage = GetConfigSettingInt("ItemsPerPage", 10);
        }

        public string MailFrom { get; }
        public string BccTo { get; }
        public string SmtpServer { get; }
        public string SmtpLogin { get; }
        public string SmtpPassword { get; }
        public string UploadFolder { get; }
        public string ResizedImagesFolder { get; }
        public int SmtpPort { get; }
        public int UploadMaxWidth { get; }
        public int UploadMaxHeight { get; }
        public int UploadQuality { get; }
        public int ItemsPerPage { get; }

        public string SendNotificationsTo { get; }

        public string GetConfigSettingString(string key, string defaultValue = "")
        {
            var value = _collection[key];
            if (value == null)
            {
                return defaultValue;
            }
            return value;
        }

        public int GetConfigSettingInt(string key, int defaultValue = 0)
        {
            if (_collection[key] == null)
            {
                return defaultValue;
            }
            string value = GetConfigSettingString(key);
            int result;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            return defaultValue;
        }
    }
}