﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Web.Mvc;
using log4net;

namespace Core.Services
{
    public class EmailService : IEmailService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IConfigService _configService;
        private readonly Postal.EmailService _postalEmailService;

        public EmailService(IConfigService configService)
        {
            _configService = configService;

            var smtpClient = GetSmtpClient();
            _postalEmailService = new Postal.EmailService(ViewEngines.Engines, () => smtpClient);
        }

        private SmtpClient GetSmtpClient()
        {
            var client = new SmtpClient
            {
                Host = _configService.SmtpServer,
                Port = _configService.SmtpPort,
                EnableSsl = true,
                Credentials = new NetworkCredential(_configService.SmtpLogin, _configService.SmtpPassword)
            };

            return client;
        }

        public void SendEmail(string mailTo, string subject, string body, string replyTo = null)
        {
            try
            {
                using (var mail = new MailMessage(_configService.MailFrom, mailTo, subject, body))
                {
                    if (!string.IsNullOrWhiteSpace(_configService.BccTo))
                    {
                        mail.Bcc.Add(_configService.BccTo);
                    }

	                if (!string.IsNullOrEmpty(replyTo))
	                {
		                mail.ReplyToList.Clear();
		                mail.ReplyToList.Add(replyTo);
	                }

	                using (var client = GetSmtpClient())
                    {
                        client.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error sending email.", ex);
                throw;
            }
        }

        public void SendEmail(dynamic email)
        {
            if (email.GetType().GetProperty("To") == null || string.IsNullOrWhiteSpace(email.To))
            {
                email.To = _configService.SendNotificationsTo;
            }
            if (email.GetType().GetProperty("From") == null || string.IsNullOrWhiteSpace(email.From))
            {
                email.From = _configService.MailFrom;
            }

            MailMessage message = _postalEmailService.CreateMailMessage(email);

            if (email.GetType().GetProperty("ReplyTo") != null && !string.IsNullOrWhiteSpace(email.ReplyTo))
            {
                var replyToList = ((string)email.ReplyTo).Split(';').Select(x => x.Trim());

                foreach (var replyToEmail in replyToList)
                {
                    message.ReplyToList.Add(replyToEmail);
                }

            }
            var client = GetSmtpClient();
            client.Send(message);
        }
    }
}
