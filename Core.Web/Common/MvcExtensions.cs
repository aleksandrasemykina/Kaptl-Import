﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using JetBrains.Annotations;

namespace Core.Web.Common
{
    public static class MvcExtensions
    {
        public static string RenderViewToString(ControllerContext context, [AspMvcPartialView] string viewName, object model)
        {
            var viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewName);

            if (viewEngineResult == null)
                throw new FileNotFoundException($"View '{viewName}' cannot be found.");

            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                    context.Controller.ViewData,
                    context.Controller.TempData,
                    sw);
                view.Render(ctx, sw);
                return sw.ToString();
            }
        }

        public static T CreateController<T>() where T : Controller, new()
        {
            var httpContextWrapper = new HttpContextWrapper(HttpContext.Current ??
                new HttpContext(new HttpRequest(null, "http://localhost/", null), new HttpResponse(null)));

            var routeData = new RouteData();
            routeData.Values.Add("controller", typeof(T).Name.Replace("Controller", string.Empty));

            var controller = new T();

            controller.ControllerContext = new ControllerContext(httpContextWrapper, routeData, controller);

            return controller;
        }
    }
}
