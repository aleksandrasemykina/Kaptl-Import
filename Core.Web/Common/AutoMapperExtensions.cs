﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Linq.Expressions;
using AutoMapper;
using JetBrains.Annotations;

namespace Core.Web.Common
{
    public static class AutoMapperExtensions
    {
        public static IMappingExpression<TSource, TDestination> ForMemberIgnore<TSource, TDestination>(
            [NotNull] this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> destinationMember)
        {
            return map.ForMember(destinationMember, config => config.Ignore());
        }

        public static IMappingExpression<TSource, TDestination> ForMemberMapFrom<TSource, TDestination, TMember>(
            [NotNull] this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> destinationMember,
            Expression<Func<TSource, TMember>> sourceMember)
        {
            return map.ForMember(destinationMember, expression => expression.MapFrom(sourceMember));
        }
    }
}
