﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using JetBrains.Annotations;

namespace Core.Web.Common
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString FieldFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, TProperty>> expression,
            [AspMvcPartialView] string templateName = null,
            object htmlAttributes = null)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var member = (MemberExpression)expression.Body;

            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (metadata.ModelType != typeof(bool) && !metadata.ModelType.IsNullableTypeOf(typeof(bool)))
                AddCssClass(attributes, "form-control");

            var label = htmlHelper.LabelFor(expression);
            var validation = htmlHelper.ValidationMessageFor(expression);

            var dropDownAttribute = member.Member.GetCustomAttributes(typeof(DropDownAttribute), false).
                FirstOrDefault() as DropDownAttribute;
            var dataTypeAttribute = member.Member.GetCustomAttributes(typeof(DataTypeAttribute), false).
                FirstOrDefault() as DataTypeAttribute;

            MvcHtmlString control;

            if (dataTypeAttribute != null && dataTypeAttribute.DataType == DataType.Password)
            {
                control = htmlHelper.PasswordFor(expression, attributes);
            }
            else if (dataTypeAttribute != null && dataTypeAttribute.DataType == DataType.MultilineText)
            {
                control = htmlHelper.TextAreaFor(expression, attributes);
            }
            else if (dataTypeAttribute != null && dataTypeAttribute.DataType == DataType.Upload)
            {
                var name = ExpressionHelper.GetExpressionText(expression);
                control = htmlHelper.FileBox(name, attributes);
            }
            else if (dropDownAttribute != null)
            {
                var name = ExpressionHelper.GetExpressionText(expression);
                control = htmlHelper.DropDownListFor(expression,
                    new SelectList((IEnumerable)htmlHelper.ViewData[name + "Dic"], dropDownAttribute.Key, dropDownAttribute.Value), attributes);
            }
            else if (metadata.ModelType.IsEnum)
            {
                control = htmlHelper.DropDownListFor(expression,
                    new SelectList(((Enum)metadata.Model).ToDictionary(), "Key", "Value"), attributes);
            }
            else if (metadata.ModelType == typeof(bool) || metadata.ModelType.IsNullableTypeOf(typeof(bool)))
            {
                var isChecked = false;
                if (metadata.Model != null)
                {
                    bool modelChecked;
                    if (bool.TryParse(metadata.Model.ToString(), out modelChecked))
                        isChecked = modelChecked;
                }
                var name = ExpressionHelper.GetExpressionText(expression);
                control = htmlHelper.CheckBox(name, isChecked, htmlAttributes);
            }
            else if (dataTypeAttribute != null && dataTypeAttribute.DataType == DataType.Date)
            {
                AddCssClass(attributes, "js-datepicker");

                var group = new TagBuilder("div");
                group.AddCssClass("input-group");
                var span = new TagBuilder("span");
                span.AddCssClass("input-group-addon");
                var glyph = new TagBuilder("span");
                glyph.AddCssClass("fa fa-calendar");
                span.InnerHtml = glyph.ToString();
                group.InnerHtml = htmlHelper.TextBoxFor(expression, "{0:MM/dd/yyyy}", attributes) + span.ToString();
                control = new MvcHtmlString(group.ToString());
            }
            else if (dataTypeAttribute != null && dataTypeAttribute.DataType == DataType.EmailAddress)
            {
                var group = new TagBuilder("div");
                group.AddCssClass("input-group");
                var span = new TagBuilder("span");
                span.AddCssClass("input-group-addon");
                var glyph = new TagBuilder("span");
                glyph.AddCssClass("fa fa-at");
                span.InnerHtml = glyph.ToString();
                group.InnerHtml = htmlHelper.TextBoxFor(expression, attributes) + span.ToString();
                control = new MvcHtmlString(group.ToString());
            }
            else
            {
                control = htmlHelper.TextBoxForEx(expression, attributes);
            }

            var partialView = htmlHelper.Partial(templateName ?? "_Field", htmlHelper.ViewData.Model);

            return MvcHtmlString.Create(string.Format(partialView.ToString(), label, control, validation));
        }

        public static void AddCssClass(RouteValueDictionary htmlAttributes, string className)
        {
            htmlAttributes["class"] = htmlAttributes["class"] + " " + className;
        }

        public static MvcHtmlString ValuedCheckboxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, TProperty>> expression, string value,
            bool @checked = false, object htmlAttributes = null)
        {
            var tag = new TagBuilder("input");
            tag.Attributes.Add("type", "checkbox");
            tag.Attributes.Add("name", htmlHelper.IdFor(expression).ToString());
            tag.Attributes.Add("value", value);
            if (@checked)
                tag.Attributes.Add("checked", string.Empty);
            tag.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString FileBox<TModel>(this HtmlHelper<TModel> htmlHelper,
            string name, object htmlAttributes = null)
        {
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            AddCssClass(attributes, "form-control");

            var tag = new TagBuilder("input");
            tag.Attributes.Add("type", "file");
            tag.Attributes.Add("name", name);
            tag.MergeAttributes(attributes);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString ValuedCheckbox<TModel>(this HtmlHelper<TModel> htmlHelper,
            string name, string value, bool @checked = false, object htmlAttributes = null)
        {
            var tag = new TagBuilder("input");
            tag.Attributes.Add("type", "checkbox");
            tag.Attributes.Add("name", name);
            tag.Attributes.Add("value", value);
            if (@checked)
                tag.Attributes.Add("checked", string.Empty);
            tag.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString TextFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, TValue>> expression,
            bool displayName = true, object htmlAttributes = null)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            var tag = new TagBuilder("span");
            tag.SetInnerText(displayName ? metadata.DisplayName : metadata.Model.ToString());
            tag.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            return MvcHtmlString.Create(tag.ToString());
        }

        public static MvcHtmlString LabeledValueFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, TValue>> expression,
            string htmlTemplateFormat = "{0} {1}", bool showEmpty = false)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            if (string.IsNullOrEmpty(metadata.Model?.ToString()) && !showEmpty)
                return MvcHtmlString.Empty;
            return MvcHtmlString.Create(string.Format(htmlTemplateFormat, metadata.DisplayName, metadata.Model));
        }

        public static MvcHtmlString Submit(this HtmlHelper htmlHelper, string buttonText,
            object htmlAttributes = null)
        {
            var tag = new TagBuilder("input");
            tag.Attributes.Add("type", "submit");
            tag.Attributes.Add("value", buttonText);
            tag.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString Link(this HtmlHelper htmlHelper, string linkText, string href,
            object htmlAttributes = null)
        {
            var tag = new TagBuilder("a");
            tag.Attributes.Add("href", href);
            tag.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            tag.InnerHtml = linkText;

            return MvcHtmlString.Create(tag.ToString());
        }

        public static MvcHtmlString ActionLinkReturnUrl(this HtmlHelper htmlHelper, string linkText,
            [AspMvcAction] string actionName, [AspMvcController] string controllerName = null,
            object routeValues = null, object htmlAttributes = null)
        {
            var returnUrl = htmlHelper.ViewContext.HttpContext.Request.QueryString["ReturnUrl"];

            return string.IsNullOrEmpty(returnUrl)
                ? htmlHelper.ActionLink(linkText, actionName, controllerName, routeValues, htmlAttributes)
                : htmlHelper.Link(linkText, returnUrl, htmlAttributes);
        }

        public static MvcHtmlString LabeledCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, bool>> expression, object htmlAttributes = null,
            string htmlTemplateFormat = "{0} {1}")
        {
            var label = htmlHelper.LabelFor(expression);
            var check = htmlHelper.CheckBoxFor(expression, htmlAttributes);

            return MvcHtmlString.Create(string.Format(htmlTemplateFormat, check, label));
        }

        public static MvcHtmlString TextBoxForEx<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, TProperty>> expression,
            object htmlAttributes = null)
        {
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            return htmlHelper.TextBoxForEx(expression, attributes);
        }

        public static MvcHtmlString AreaLink<TModel>(
            this HtmlHelper<TModel> htmlHelper,
            [NotNull] string linkText,
            [NotNull, AspMvcAction] string actionName,
            [NotNull, AspMvcController] string controllerName,
            [AspMvcArea] string areaName = null,
            object htmlAttributes = null)
        {
            return htmlHelper.ActionLink(linkText, actionName, controllerName,
                new { area = areaName ?? string.Empty }, htmlAttributes ?? new {});
        }

        public static MvcHtmlString TextBoxForEx<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, TProperty>> expression,
            IDictionary<string, object> htmlAttributes)
        {
            var member = (MemberExpression)expression.Body;

            var maxLengthAttribute = member.Member.GetCustomAttributes(typeof(InputLengthAttribute), false).FirstOrDefault() as InputLengthAttribute;
            if (maxLengthAttribute != null)
                htmlAttributes.Add("maxlength", maxLengthAttribute.MaximumLength);
            else
            {
                var stringLengthAttribute = member.Member.GetCustomAttributes(typeof(StringLengthAttribute), false).FirstOrDefault() as StringLengthAttribute;
                if (stringLengthAttribute != null)
                    htmlAttributes.Add("maxlength", stringLengthAttribute.MaximumLength);
            }

            return htmlHelper.TextBoxFor(expression, htmlAttributes);
        }

        public static MvcHtmlString RadioButtonGroupFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            [NotNull] Expression<Func<TModel, TProperty>> expression,
            [CanBeNull] IEnumerable<KeyValuePair<string, string>> pairs,
            string htmlTemplateFormat = "{0} {1}")
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            var sb = new StringBuilder();

            if (pairs != null)
                foreach (var item in pairs)
                {
                    var id = $"{metadata.PropertyName}_{item.Key}";

                    var label = htmlHelper.Label(id, item.Value);
                    var radio = htmlHelper.RadioButtonFor(expression, item.Key, new {id});

                    sb.AppendFormat(htmlTemplateFormat, radio, label);
                }

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}
