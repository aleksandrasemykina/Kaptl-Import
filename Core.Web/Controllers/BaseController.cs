﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using JetBrains.Annotations;
using log4net;

namespace Core.Web.Controllers
{
    [ValidateInput(false)]
    public abstract class BaseController : Controller
    {
        private const string ModelStateParamName = "ModelState";

        public static ILog Log { get; } = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected ActionResult DefaultActionResult { get; set; }

        public void SaveModelState()
        {
            if (!ModelState.IsValid)
                TempData[ModelStateParamName] = ModelState;
        }

        private void RestoreModelState()
        {
            var modelState = TempData[ModelStateParamName] as ModelStateDictionary;
            if (modelState != null && !ModelState.Equals(modelState))
            {
                if (modelState[""]?.Errors != null)
                {
                    foreach (var error in modelState[""].Errors)
                    {
                        modelState[""].Errors.Add(error);
                    }
                }
                ModelState["info"] = modelState["info"];
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (DefaultActionResult != null)
            {
                filterContext.Result = DefaultActionResult;
                return;
            }

            RestoreModelState();
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
//            if (!filterContext.IsChildAction)
//                Response.Write(@"<div style='position: fixed; left: 0; bottom: 0'>Buy License</div>");
        }

        public virtual void HandleException([NotNull] Exception exception, params Type[] types)
        {
            var userErrorException = exception as UserErrorException;
            var dbEntityValidationException = exception as DbEntityValidationException;

            if (userErrorException == null && dbEntityValidationException == null)
                if (types == null || !types.Contains(exception.GetType()))
                    throw new HttpUnhandledException(exception.Message, exception);

            if (dbEntityValidationException != null)
            {
                foreach (var error in dbEntityValidationException.EntityValidationErrors.
                    SelectMany(entityValidationResult => entityValidationResult.ValidationErrors))
                {
                    ModelState.AddModelError(string.Empty, error.ErrorMessage);
                }
            } else
                ModelState.AddModelError(string.Empty, exception.Message);

            SaveModelState();

            if (userErrorException?.ActionResult != null)
                DefaultActionResult = userErrorException.ActionResult;
        }

        public void AddInfoMessage(string message)
        {
            ModelState.AddModelError("info", message);
            SaveModelState();
        }

        protected void InitEntityDic(string name, IList<KeyValuePair<string, string>> list)
        {
            list.Insert(0, new KeyValuePair<string, string>(null, "---- None ----"));
            ViewData[name] = list;
        }

        public ActionResult RedirectToReturnUrlOrAction([AspMvcAction] string actionName, [AspMvcController] string controllerName = null)
        {
            var returnUrl = Request.QueryString["ReturnUrl"];

            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            if (string.IsNullOrEmpty(controllerName))
                return RedirectToAction(actionName);

            return RedirectToAction(actionName, controllerName);
        }
    }
}
