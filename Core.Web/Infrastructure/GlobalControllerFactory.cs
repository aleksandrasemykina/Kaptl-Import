﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Core.Web.Filters;

namespace Core.Web.Infrastructure
{
    public class GlobalControllerFactory : DefaultControllerFactory
    {
        protected override SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null || requestContext.RouteData.Values["action"] == null)
                return base.GetControllerSessionBehavior(requestContext, controllerType);

            var actionName = requestContext.RouteData.Values["action"].ToString();

            MethodInfo actionMethodInfo = null;
            if (requestContext.HttpContext.Request.RequestType.Equals("POST", StringComparison.OrdinalIgnoreCase))
                actionMethodInfo = controllerType.GetMethods().FirstOrDefault(
                    methodInfo => methodInfo.Name.Equals(actionName) &&
                                  methodInfo.GetCustomAttributes(typeof(HttpPostAttribute), false).Length != 0);

            if (actionMethodInfo == null)
                actionMethodInfo = controllerType.GetMethods().FirstOrDefault(
                    methodInfo => methodInfo.Name.Equals(actionName) &&
                                  methodInfo.GetCustomAttributes(typeof(HttpPostAttribute), false).Length == 0);

            var attr = actionMethodInfo?.GetCustomAttributes(typeof(ActionSessionStateAttribute), false).
                OfType<ActionSessionStateAttribute>().FirstOrDefault();
            if (attr != null)
                return attr.Behavior;

            return base.GetControllerSessionBehavior(requestContext, controllerType);
        }
    }
}
