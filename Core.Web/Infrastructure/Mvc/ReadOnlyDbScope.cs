﻿using System.Web.Mvc;
using Mehdime.Entity;
using Microsoft.Practices.Unity;

namespace Core.Web.Infrastructure.Mvc
{
    public class ReadOnlyDbScopeAttribute : ActionFilterAttribute
    {
        private IDbContextScopeFactory _scopeFactory;

        [Dependency]
        public IDbContextScopeFactory ScopeFactory
        {
            get { return _scopeFactory; }
            set { _scopeFactory = value; }
        }
        private IDbContextReadOnlyScope _scope;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _scope = _scopeFactory.CreateReadOnly();
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            _scope.Dispose();
        }
    }
}
