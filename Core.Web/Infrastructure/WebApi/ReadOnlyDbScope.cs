﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Mehdime.Entity;
using Microsoft.Practices.Unity;

namespace Core.Web.Infrastructure.WebApi
{
    public class ReadOnlyDbScopeAttribute : ActionFilterAttribute
    {
        private IDbContextScopeFactory _scopeFactory;

        [Dependency]
        public IDbContextScopeFactory ScopeFactory
        {
            get { return _scopeFactory; }
            set { _scopeFactory = value; }
        }
        private IDbContextReadOnlyScope _scope;


        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            _scope = _scopeFactory.CreateReadOnly();
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            _scope.Dispose();
        }

    }
}
