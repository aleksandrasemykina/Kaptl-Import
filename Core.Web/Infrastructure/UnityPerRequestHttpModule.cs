﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.Web.Infrastructure;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;

[assembly: PreApplicationStartMethod(typeof(UnityPerRequestHttpModule), "Start")]

namespace Core.Web.Infrastructure
{
    public class UnityPerRequestHttpModule : IHttpModule
    {
        private static readonly object ModuleKey = new object();

        public static object GetValue(object lifetimeManagerKey)
        {
            var dictionary = GetDictionary(HttpContext.Current);
            if (dictionary == null)
                return null;
            object obj;
            return dictionary.TryGetValue(lifetimeManagerKey, out obj) ? obj : null;
        }

        public static void SetValue(object lifetimeManagerKey, object value)
        {
            var dictionary = GetDictionary(HttpContext.Current);
            if (dictionary == null)
            {
                dictionary = new Dictionary<object, object>();
                HttpContext.Current.Items[ModuleKey] = dictionary;
            }
            dictionary[lifetimeManagerKey] = value;
        }

        public void Init(HttpApplication context)
        {
            context.EndRequest += OnEndRequest;
        }

        public void Dispose()
        {
        }

        private static void OnEndRequest(object sender, EventArgs e)
        {
            var dictionary = GetDictionary(((HttpApplication) sender).Context);
            if (dictionary == null)
                return;

            foreach (var disposable in dictionary.Values.OfType<IDisposable>())
                disposable.Dispose();
        }

        private static Dictionary<object, object> GetDictionary(HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            return (Dictionary<object, object>) context.Items[ModuleKey];
        }

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
        }
    }
}