/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Web.Mvc;
using JetBrains.Annotations;
using Microsoft.Practices.Unity;

namespace Core.Web.Infrastructure
{
    public class UnityMvcFilterProvider : FilterAttributeFilterProvider
    {
        private readonly IUnityContainer _container;

        public UnityMvcFilterProvider([NotNull] IUnityContainer container)
        {
            _container = container;
        }

        protected override IEnumerable<FilterAttribute> GetControllerAttributes(ControllerContext controllerContext,
            ActionDescriptor actionDescriptor)
        {
            var attrs = base.GetControllerAttributes(controllerContext, actionDescriptor);

            // ReSharper disable PossibleMultipleEnumeration
            BuildUpAttributes(attrs);
            return attrs;
            // ReSharper restore PossibleMultipleEnumeration
        }

        protected override IEnumerable<FilterAttribute> GetActionAttributes(ControllerContext controllerContext,
            ActionDescriptor actionDescriptor)
        {
            var attrs = base.GetActionAttributes(controllerContext, actionDescriptor);

            // ReSharper disable PossibleMultipleEnumeration
            BuildUpAttributes(attrs);
            return attrs;
            // ReSharper restore PossibleMultipleEnumeration
        }

        private void BuildUpAttributes(IEnumerable<FilterAttribute> attrs)
        {
            foreach (var attribute in attrs)
                _container.BuildUp(attribute.GetType(), attribute);
        }
    }
}
