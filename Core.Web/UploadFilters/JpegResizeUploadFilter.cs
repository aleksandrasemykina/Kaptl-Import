﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Core.Services;
using Core.Web.Services;
using ImageProcessor;
using ImageProcessor.Imaging;

namespace Core.Web.UploadFilters
{
    public class JpegResizeUploadFilter : IUploadFilter
    {
        private readonly IConfigService _configService;

        public JpegResizeUploadFilter(IConfigService configService)
        {
            _configService = configService;
        }

        public void Apply(UploadFilterContext context)
        {
            var maxWidth = _configService.UploadMaxWidth;
            var maxHeight = _configService.UploadMaxHeight;
            var quality = _configService.UploadQuality;

            if (!context.ContentType.EndsWith("jpeg") || (maxHeight == 0 && maxWidth == 0))
            {
                return;
            }
            var imageStream = context.Stream;
            var resizedImageStream = new MemoryStream();
            var size = new Size(maxWidth, maxHeight);

            var resizeLayer = new ResizeLayer(size, ResizeMode.Max);
            using (var imageFactory = new ImageFactory(true))
            {
                imageFactory
                    .Load(imageStream)
                    .Resize(resizeLayer)
                    .Quality(quality)
                    .Save(resizedImageStream);
            }
            context.Stream = resizedImageStream;
            context.ContentType = context.ContentType;
            context.FileName = context.FileName;
        }
    }
}
