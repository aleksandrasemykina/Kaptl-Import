﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Web.UploadFilters
{
    public interface IUploadFilter
    {
        void Apply(UploadFilterContext context);
    }
}
