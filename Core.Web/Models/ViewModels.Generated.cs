using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Core.Data.DomainModel;
using Core.Web.Common;
using Core.Data;
using System.Web.Mvc;

namespace Core.Web.Models
{
    public partial class RoleViewModel
    {
        public RoleViewModel()
        {
        }
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }

    public partial class RoleListItemViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Display(Name = "Name")]
        public string Name { get; set; }
    }

    public class RoleViewModelProfile : AutoMapper.Profile
    {
        public RoleViewModelProfile() : base("RoleViewModelProfile")
        {
        }

        protected override void Configure()
        {
            CreateMap<Core.Data.DomainModel.Role, RoleViewModel>();
            CreateMap<Core.Data.DomainModel.Role, RoleListItemViewModel>();
            CreateMap<RoleViewModel, Core.Data.DomainModel.Role>();
        }
    }

    public partial class UserViewModel
    {
        public UserViewModel()
        {
        }
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Email Confirmed")]
        public bool EmailConfirmed { get; set; }

        [Required]
        [Display(Name = "Phone Number Confirmed")]
        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        [Display(Name = "Two Factor Enabled")]
        public bool TwoFactorEnabled { get; set; }

        [Required]
        [Display(Name = "Lockout Enabled")]
        public bool LockoutEnabled { get; set; }
    }

    public partial class UserListItemViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Email Confirmed")]
        public bool EmailConfirmed { get; set; }

        [Display(Name = "Phone Number Confirmed")]
        public bool PhoneNumberConfirmed { get; set; }

        [Display(Name = "Two Factor Enabled")]
        public bool TwoFactorEnabled { get; set; }

        [Display(Name = "Lockout Enabled")]
        public bool LockoutEnabled { get; set; }
    }

    public class UserViewModelProfile : AutoMapper.Profile
    {
        public UserViewModelProfile() : base("UserViewModelProfile")
        {
        }

        protected override void Configure()
        {
            CreateMap<Core.Data.DomainModel.User, UserViewModel>()
                .ForMemberIgnore(model => model.Roles);
            CreateMap<Core.Data.DomainModel.User, UserListItemViewModel>();
            CreateMap<UserViewModel, Core.Data.DomainModel.User>()
                .ForMemberIgnore(model => model.Roles);
        }
    }

    public partial class EmployeeViewModel
    {
        public EmployeeViewModel()
        {
        }
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [UIHint("KaptlImage")]
        [Display(Name = "Img")]
        public string Img { get; set; }
        public KaptlMedia ImgMedia { get; set; }

        [UIHint("KaptlUpload")]
        [Display(Name = "Employee File")]
        public string EmployeeFile { get; set; }
        public KaptlMedia EmployeeFileMedia { get; set; }

        [Required]
        [UIHint("KaptlGallery")]
        [Display(Name = "Employee Gallery")]
        public string EmployeeGallery { get; set; }
        public IEnumerable<KaptlMedia> EmployeeGalleryMediaList { get; set; }

        [Required]
        [UIHint("KaptlFileList")]
        [Display(Name = "Employee File List")]
        public string EmployeeFileList { get; set; }
        public IEnumerable<KaptlMedia> EmployeeFileListMediaList { get; set; }

        public IList<ProjectViewModel> Projects { get; set; }
        public IList<int> ProjectIds { get; set; }
    }

    public partial class EmployeeListItemViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Img")]
        public string Img { get; set; }
        public KaptlMedia ImgMedia { get; set; }

        [Display(Name = "Employee File")]
        public string EmployeeFile { get; set; }
        public KaptlMedia EmployeeFileMedia { get; set; }

        [Display(Name = "Employee Gallery")]
        public string EmployeeGallery { get; set; }
        public IEnumerable<KaptlMedia> EmployeeGalleryMediaList { get; set; }

        [Display(Name = "Employee File List")]
        public string EmployeeFileList { get; set; }
        public IEnumerable<KaptlMedia> EmployeeFileListMediaList { get; set; }
    }

    public class EmployeeViewModelProfile : AutoMapper.Profile
    {
        public EmployeeViewModelProfile() : base("EmployeeViewModelProfile")
        {
        }

        protected override void Configure()
        {
            CreateMap<Core.Data.DomainModel.Employee, EmployeeViewModel>()
                .ForMemberIgnore(model => model.Projects);
            CreateMap<Core.Data.DomainModel.Employee, EmployeeListItemViewModel>();
            CreateMap<EmployeeViewModel, Core.Data.DomainModel.Employee>()
                .ForMemberIgnore(model => model.ImgMedia)
                .ForMemberIgnore(model => model.EmployeeFileMedia)
                .ForMemberIgnore(model => model.EmployeeGalleryMediaList)
                .ForMemberIgnore(model => model.EmployeeFileListMediaList)
                .ForMemberIgnore(model => model.Projects);
        }
    }

    public partial class ProjectViewModel
    {
        public ProjectViewModel()
        {
        }
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        public IList<EmployeeViewModel> Employees { get; set; }
        public IList<int> EmployeeIds { get; set; }

        [Required]
        [Display(Name = "Number")]
        public string Number { get; set; }

        [Required]
        [UIHint("KaptlGallery")]
        [Display(Name = "Project Gallery")]
        public string ProjectGallery { get; set; }
        public IEnumerable<KaptlMedia> ProjectGalleryMediaList { get; set; }
    }

    public partial class ProjectListItemViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Display(Name = "Number")]
        public string Number { get; set; }

        [Display(Name = "Project Gallery")]
        public string ProjectGallery { get; set; }
        public IEnumerable<KaptlMedia> ProjectGalleryMediaList { get; set; }
    }

    public class ProjectViewModelProfile : AutoMapper.Profile
    {
        public ProjectViewModelProfile() : base("ProjectViewModelProfile")
        {
        }

        protected override void Configure()
        {
            CreateMap<Core.Data.DomainModel.Project, ProjectViewModel>()
                .ForMemberIgnore(model => model.Employees);
            CreateMap<Core.Data.DomainModel.Project, ProjectListItemViewModel>();
            CreateMap<ProjectViewModel, Core.Data.DomainModel.Project>()
                .ForMemberIgnore(model => model.Employees)
                .ForMemberIgnore(model => model.ProjectGalleryMediaList);
        }
    }

    public partial class ConfigViewModel
    {
        public ConfigViewModel()
        {
        }
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Required]
        [Display(Name = "Key")]
        public string Key { get; set; }

        [Required]
        [Display(Name = "Value")]
        public string Value { get; set; }
    }

    public partial class ConfigListItemViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        public Guid Identity { get; set; } = Guid.NewGuid();

        [Display(Name = "Key")]
        public string Key { get; set; }

        [Display(Name = "Value")]
        public string Value { get; set; }
    }

    public class ConfigViewModelProfile : AutoMapper.Profile
    {
        public ConfigViewModelProfile() : base("ConfigViewModelProfile")
        {
        }

        protected override void Configure()
        {
            CreateMap<Core.Data.DomainModel.Config, ConfigViewModel>();
            CreateMap<Core.Data.DomainModel.Config, ConfigListItemViewModel>();
            CreateMap<ConfigViewModel, Core.Data.DomainModel.Config>();
        }
    }

}
