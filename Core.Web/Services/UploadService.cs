﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using Core.Data;
using Core.Services;
using Core.Web.UploadFilters;

namespace Core.Web.Services
{
    public interface IUploadService
    {
        KaptlMedia Upload(Stream stream, string fileName, string contentType);
        string GetPublicUrl(string fileId);
    }

    public class UploadService : IUploadService
    {

        private readonly IFileStorageService _fileStorageService;
        private readonly IUploadFilter[] _filters;

        public UploadService(IFileStorageService fileStorageService, IUploadFilter[] filters)
        {
            _fileStorageService = fileStorageService;
            _filters = filters;
        }

        public KaptlMedia Upload(Stream stream, string fileName, string contentType)
        {
            var context = new UploadFilterContext
            {
                Stream = stream,
                FileName = fileName,
                ContentType = contentType
            };

            foreach (var filter in _filters)
            {
                filter.Apply(context);
            }

            var fileId = GenerateFileId(context.FileName);
            fileId = _fileStorageService.Upload(context.Stream, fileId);
            return new KaptlMedia()
            {
                Id = fileId,
                Name = context.FileName,
                PublicUrl = GetPublicUrl(fileId),
                ContentType = context.ContentType,
                FileSize = context.Stream.Length
            };
        }

        protected virtual string GenerateFileId(string fileName)
        {
            fileName = Regex.Replace(fileName, @"\s", "_");
            if (!_fileStorageService.Exists(fileName))
            {
                return fileName;
            }
            var fileExtension = Path.GetExtension(fileName);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            for (var i = 1; i < 10; i++)
            {
                var nextFileName = fileNameWithoutExtension + "-" + i + fileExtension;
                if (!_fileStorageService.Exists(nextFileName))
                {
                    return nextFileName;
                }
            }
            var fileId = Guid.NewGuid().ToString("N") + fileExtension;
            return fileId;
        }

        public string GetPublicUrl(string fileId)
        {
            return _fileStorageService.GetUrl(fileId);
        }
    }
}