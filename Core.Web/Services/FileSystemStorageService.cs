﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;
using Core.Services;

namespace Core.Web.Services
{
    public interface IFileStorageService
    {
        string Upload(Stream stream, string key, bool asPublic = true);
        Stream Download(string key);
        void Delete(string key);
        string GetUrl(string key);
        bool Exists(string key);

        IEnumerable<string> AllKeys();
    }

    public class FileSystemStorageService : IFileStorageService
    {
        private readonly IConfigService _configService;
        private readonly string _uploadFolderServerPath;

        public FileSystemStorageService(IConfigService configService)
        {
            _configService = configService;
            _uploadFolderServerPath = GetServerPath(_configService.UploadFolder);
        }

        public string Upload(Stream stream, string key, bool asPublic = false)
        {
            var filePath = GetServerPathForKey(key);
            EnsureKeyFolders(filePath);
            using (var fileStream = File.Create(filePath))
            {
                if (stream.CanSeek)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                }
                stream.CopyTo(fileStream);
            }
            return key;
        }

        public Stream Download(string key)
        {
            if (!Exists(key))
            {
                return null;
            }
            var serverPath = GetServerPathForKey(key);
            return File.OpenRead(serverPath);
        }

        public void Delete(string key)
        {
            File.Delete(GetServerPathForKey(key));
        }

        public string GetUrl(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                return string.Empty;
            key = CleanKey(key);
            key = Path.Combine(_configService.UploadFolder, key);
            return VirtualPathUtility.ToAbsolute(key);
        }

        private static void EnsureKeyFolders(string path)
        {
            var folder = Path.GetDirectoryName(path);
            if (folder != null && !Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
        }

        public bool Exists(string key)
        {
            key = CleanKey(key);
            var serverPath = GetServerPathForKey(key);
            return File.Exists(serverPath);
        }

        public IEnumerable<string> AllKeys()
        {
            return Directory.GetFiles(_uploadFolderServerPath).Select(x => Path.GetFileName(x)).ToList();
        }

        private string GetServerPathForKey(string key)
        {
            key = CleanKey(key);
            return Path.Combine(_uploadFolderServerPath, key);
        }

        private string CleanKey(string key)
        {
            var rootPath = "~/" + _configService.UploadFolder.TrimStart('/');
            if (key.StartsWith(rootPath, StringComparison.OrdinalIgnoreCase))
            {
                key = key.Substring(rootPath.Length);
            }
            key = Regex.Replace(key.Trim(), "^~/", string.Empty);
            key = key.TrimStart('/');
            return key;
        }

        private string GetServerPath(string path)
        {
            path = CleanKey(path);
            return HostingEnvironment.IsHosted
                ? HostingEnvironment.MapPath("~/" + path) ?? string.Empty
                : Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
        }
    }
}
