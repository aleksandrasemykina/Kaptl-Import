﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Core.Services;
using ImageProcessor;
using ImageProcessor.Imaging;
using log4net;

namespace Core.Web.Services
{
    public interface IMediaProcessorService
    {
        string GetImageUrl(string path, int width = 0, int height = 0, string settings = "");
    }

    public class MediaProcessorService : IMediaProcessorService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IConfigService _configService;
        private readonly IFileStorageService _fileStorageService;
        private readonly string _resizedImagesFolder;

        public MediaProcessorService(IFileStorageService fileStorageService, IConfigService configService)
        {
            _configService = configService;
            _fileStorageService = fileStorageService;

            _resizedImagesFolder = configService.ResizedImagesFolder.Trim().TrimStart('~');
        }

        public string GetImageUrl(string path, int width = 0, int height = 0, string settings = "")
        {
            try
            {
                path = path.Trim();

                if (!_fileStorageService.Exists(path))
                {
                    return string.Empty;
                }

                var settingsDictionary = GetSettings(settings);

                var resizedFileName = GetResizedFileName(path, width, height, settings);
                var resizedImagePath = Path.Combine(_resizedImagesFolder, resizedFileName);

                if (_fileStorageService.Exists(resizedImagePath))
                {
                    return _fileStorageService.GetUrl(resizedImagePath);
                }

                using (var imageStream = _fileStorageService.Download(path))
                {
                    using (var resizedImageStream = new MemoryStream())
                    {
                        var resizeLayer = GetResizeLayer(width, height, settingsDictionary);
                        var quality = GetIntSetting(settingsDictionary, 89, "q", "quality", "jpegquality");
                        using (var imageFactory = new ImageFactory(true))
                        {
                            imageFactory
                                .Load(imageStream)
                                .Resize(resizeLayer)
                                .Quality(quality)
                                .Save(resizedImageStream);
                        }
                        var key = _fileStorageService.Upload(resizedImageStream, resizedImagePath);
                        return _fileStorageService.GetUrl(key);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Resizing error", e);
                throw;
            }
        }

        private static string GetResizedFileName(string path, int width, int height, string settings)
        {
            path = Regex.Replace(path, "^~/", string.Empty);
            var resizedImageFolder = GenerateResizedImageSubfolderName(Path.GetDirectoryName(path));
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);
            var settingsKey = GetSettingsKey(width, height, settings);
            var fileExtension = Path.GetExtension(path);

            var resizedFileName = $"{resizedImageFolder}/{fileNameWithoutExtension}-{settingsKey}{fileExtension}";
            return resizedFileName.TrimStart('/');
        }

        private static string GenerateResizedImageSubfolderName(string imageDir)
        {
            imageDir = imageDir
                .Trim('/', '\\', ' ')
                .Replace('\\', '/');
            imageDir = Regex.Replace(imageDir, "^content", string.Empty, RegexOptions.IgnoreCase).Trim('/', '\\', ' ');
            return imageDir;
        }

        private static string GetSettingsKey(int width, int height, string settings)
        {
            settings += $"&w={width}&h={height}";
            return CreateMd5Hash(settings);
        }

        private static string CreateMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hashBytes = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (var t in hashBytes)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }

        private static ResizeLayer GetResizeLayer(int w, int h, Dictionary<string, string> settingsDictionary)
        {
            var width = GetIntSetting(settingsDictionary, w, "w", "width");
            var height = GetIntSetting(settingsDictionary, h, "h", "height");
            var alignment = GetStringSetting(settingsDictionary, "a", "alignment", "anchor");
            var mode = GetStringSetting(settingsDictionary, "m", "mode");
            var upscale = GetBoolSetting(settingsDictionary, "u", "upscale", "isupscale");

            var size = new Size(width, height);
            var result = new ResizeLayer(size)
            {
                Upscale = upscale,
                ResizeMode = ResizeMode.Crop,
                AnchorPosition = AnchorPosition.Center
            };

            switch (mode)
            {
                case "max":
                    result.ResizeMode = ResizeMode.Max;
                    break;
                case "pad":
                    result.ResizeMode = ResizeMode.Pad;
                    break;
                case "crop":
                    result.ResizeMode = ResizeMode.Crop;
                    break;
                case "stretch":
                    result.ResizeMode = ResizeMode.Stretch;
                    break;
            }

            switch (alignment)
            {
                case "top":
                    result.AnchorPosition = AnchorPosition.Top;
                    break;
                case "left":
                    result.AnchorPosition = AnchorPosition.Left;
                    break;
                case "right":
                    result.AnchorPosition = AnchorPosition.Right;
                    break;
                case "bottom":
                    result.AnchorPosition = AnchorPosition.Bottom;
                    break;
                case "center":
                case "middle":
                    result.AnchorPosition = AnchorPosition.Center;
                    break;
                case "topleft":
                    result.AnchorPosition = AnchorPosition.Top | AnchorPosition.Left;
                    break;
                case "topcenter":
                    result.AnchorPosition = AnchorPosition.Top | AnchorPosition.Center;
                    break;
                case "topright":
                    result.AnchorPosition = AnchorPosition.Top | AnchorPosition.Right;
                    break;
                case "middleleft":
                    result.AnchorPosition = AnchorPosition.Center | AnchorPosition.Left;
                    break;
                case "middlecenter":
                    result.AnchorPosition = AnchorPosition.Center;
                    break;
                case "middleright":
                    result.AnchorPosition = AnchorPosition.Center | AnchorPosition.Right;
                    break;
                case "bottomleft":
                    result.AnchorPosition = AnchorPosition.Bottom | AnchorPosition.Left;
                    break;
                case "bottomcenter":
                    result.AnchorPosition = AnchorPosition.Bottom | AnchorPosition.Center;
                    break;
                case "bottomright":
                    result.AnchorPosition = AnchorPosition.Bottom | AnchorPosition.Right;
                    break;
            }

            return result;
        }

        private static int GetIntSetting(Dictionary<string, string> settings, int defaultValue = 0,
            params string[] optionNames)
        {
            var strValue = GetStringSetting(settings, optionNames);
            if (string.IsNullOrWhiteSpace(strValue))
                return defaultValue;
            int result;
            return int.TryParse(strValue, out result) ? result : defaultValue;
        }

        private static bool GetBoolSetting(Dictionary<string, string> settings,
            params string[] optionNames)
        {
            var strValue = GetStringSetting(settings, optionNames);
            if (string.IsNullOrWhiteSpace(strValue))
                return false;
            strValue = strValue.Trim().ToLowerInvariant();
            return strValue == "true" || strValue == "1" || strValue == "yes";
        }

        private static double GetDoubleSetting(Dictionary<string, string> settings, double defaultValue = 0,
            params string[] optionNames)
        {
            var strValue = GetStringSetting(settings, optionNames);
            if (string.IsNullOrWhiteSpace(strValue))
                return defaultValue;
            double result;
            return double.TryParse(strValue, out result) ? result : defaultValue;
        }

        private static string GetStringSetting(Dictionary<string, string> settings, params string[] optionNames)
        {
            foreach (var optionName in optionNames)
            {
                if (settings.ContainsKey(optionName))
                {
                    return settings[optionName];
                }
            }
            return "";
        }

        private static Dictionary<string, string> GetSettings(string query)
        {
            var setting = query.Split('&');
            var result = new Dictionary<string, string>();
            foreach (var option in setting.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Split('=')))
            {
                result.Add(option[0].ToLower(), option[1].ToLower());
            }
            return result;
        }
    }
}