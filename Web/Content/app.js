(function($) {
    "use strict";

    $.blockUI.defaults.message = "<i class=\"fa fa-spinner fa-spin fa-4x\"></i>";

    $.blockUI.defaults.css = {
        padding: 0,
        margin: 0,
        width: "30%",
        top: "40%",
        left: "35%",
        textAlign: "center",
        color: "#fff",
        border: "none",
        backgroundColor: "transparent",
        cursor: "wait"
    };

    $.ajaxPrefilter(function(options) {
        var token = $("input[name^=__RequestVerificationToken]").val();
        options.headers = { RequestVerificationToken: token };
    });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    window.onbeforeunload = function() {
        $.blockUI();
    };

})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRlZmF1bHQuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigkKSB7XHJcbiAgICBcInVzZSBzdHJpY3RcIjtcclxuXHJcbiAgICAkLmJsb2NrVUkuZGVmYXVsdHMubWVzc2FnZSA9IFwiPGkgY2xhc3M9XFxcImZhIGZhLXNwaW5uZXIgZmEtc3BpbiBmYS00eFxcXCI+PC9pPlwiO1xyXG5cclxuICAgICQuYmxvY2tVSS5kZWZhdWx0cy5jc3MgPSB7XHJcbiAgICAgICAgcGFkZGluZzogMCxcclxuICAgICAgICBtYXJnaW46IDAsXHJcbiAgICAgICAgd2lkdGg6IFwiMzAlXCIsXHJcbiAgICAgICAgdG9wOiBcIjQwJVwiLFxyXG4gICAgICAgIGxlZnQ6IFwiMzUlXCIsXHJcbiAgICAgICAgdGV4dEFsaWduOiBcImNlbnRlclwiLFxyXG4gICAgICAgIGNvbG9yOiBcIiNmZmZcIixcclxuICAgICAgICBib3JkZXI6IFwibm9uZVwiLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCJ0cmFuc3BhcmVudFwiLFxyXG4gICAgICAgIGN1cnNvcjogXCJ3YWl0XCJcclxuICAgIH07XHJcblxyXG4gICAgJC5hamF4UHJlZmlsdGVyKGZ1bmN0aW9uKG9wdGlvbnMpIHtcclxuICAgICAgICB2YXIgdG9rZW4gPSAkKFwiaW5wdXRbbmFtZV49X19SZXF1ZXN0VmVyaWZpY2F0aW9uVG9rZW5dXCIpLnZhbCgpO1xyXG4gICAgICAgIG9wdGlvbnMuaGVhZGVycyA9IHsgUmVxdWVzdFZlcmlmaWNhdGlvblRva2VuOiB0b2tlbiB9O1xyXG4gICAgfSk7XHJcblxyXG4gICAgJChkb2N1bWVudCkuYWpheFN0YXJ0KCQuYmxvY2tVSSkuYWpheFN0b3AoJC51bmJsb2NrVUkpO1xyXG5cclxuICAgIHdpbmRvdy5vbmJlZm9yZXVubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICQuYmxvY2tVSSgpO1xyXG4gICAgfTtcclxuXHJcbn0pKGpRdWVyeSk7Il19
