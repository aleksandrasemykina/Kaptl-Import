(function($) {
    "use strict";

    $.blockUI.defaults.message = "<i class=\"fa fa-spinner fa-spin fa-4x\"></i>";

    $.blockUI.defaults.css = {
        padding: 0,
        margin: 0,
        width: "30%",
        top: "40%",
        left: "35%",
        textAlign: "center",
        color: "#fff",
        border: "none",
        backgroundColor: "transparent",
        cursor: "wait"
    };

    $.ajaxPrefilter(function(options) {
        var token = $("input[name^=__RequestVerificationToken]").val();
        options.headers = { RequestVerificationToken: token };
    });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    window.onbeforeunload = function() {
        $.blockUI();
    };

})(jQuery);