var merge = require('merge');

    var options = {
    debug: process.env.BUILD ? process.env.BUILD === "Debug" : true,

        contentFolder: './Areas/kaptl/Content',
        bowerFolder: './Areas/kaptl/Content/bower_components',
        bowerJson: './Areas/kaptl/Content/bower.json',
        jsFolder: './Areas/kaptl/Content/js'
    };

    var wiredep = require('wiredep')({ directory: options.bowerFolder, bowerJson: require(options.bowerJson) });

    var config = {
        js: [
        options.jsFolder + '/**/*.module.js',
        options.jsFolder + '/**/templates.js',
        options.jsFolder + '/**/*.config.js',
        options.jsFolder + '/**/*.service.js',
        options.jsFolder + '/**/*.factory.js',
        options.jsFolder + '/**/*.controller.js',
        options.jsFolder + '/**/*.directive.js',
        options.jsFolder + '/**/*.js',
        '!' + options.jsFolder + '/**/*.spec.js'
        ],

        bowerCss: function () {
            if (!wiredep.css) {
                wiredep.css = [];
            }
            return wiredep.css.concat([
                options.bowerFolder + '/bootstrap/dist/css/bootstrap.css',
                options.bowerFolder + '/bootstrap/dist/css/bootstrap-theme.css',
                options.bowerFolder + '/font-awesome/css/font-awesome.min.css',
                options.contentFolder + '/components/mvc-grid/**/*.css'
            ]);
        },

        bowerJs: function () {
            if (!wiredep.js) {
                wiredep.js = [];
            }
            return wiredep.js.concat([
                options.contentFolder + '/components/mvc-grid/**/*.js',
                options.contentFolder + '/components/tinymce-dist/**/tinymce.js'
            ]);
        }
    }

module.exports = merge(config, options);
