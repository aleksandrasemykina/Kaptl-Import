using System.Collections.Generic;

namespace Web.Services
{
    public interface IContentUtilsService
    {
        IEnumerable<string> GetUnlinkedMediaKeys();
        void SetIdentities();
    }
}