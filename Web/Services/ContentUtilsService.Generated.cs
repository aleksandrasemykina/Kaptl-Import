﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.DataAccess;
using Core.Web.Services;
using CsvHelper;
using Mehdime.Entity;
using Newtonsoft.Json;

namespace Web.Services
{
    public partial class ContentUtilsService : IContentUtilsService
    {
        protected readonly IDbContextScopeFactory _dbContextScopeFactory;
        protected readonly IUploadService _uploadService;
        protected readonly IFileStorageService _fileStorageService;
        protected readonly IMapper _mapper;

        protected readonly IRoleRepository _roleRepository;
        protected readonly IUserRepository _userRepository;
        protected readonly IEmployeeRepository _employeeRepository;
        protected readonly IProjectRepository _projectRepository;
        protected readonly IConfigRepository _configRepository;

        public ContentUtilsService(
            IFileStorageService fileStorageService,
            IDbContextScopeFactory dbContextScopeFactory,
            IUploadService uploadService,
            IRoleRepository roleRepository,
            IUserRepository userRepository,
            IEmployeeRepository employeeRepository,
            IProjectRepository projectRepository,
            IConfigRepository configRepository,
            IMapper mapper
            )
        {
            _fileStorageService = fileStorageService;
            _dbContextScopeFactory = dbContextScopeFactory;
            _uploadService = uploadService;
            _mapper = mapper;
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _employeeRepository = employeeRepository;
            _projectRepository = projectRepository;
            _configRepository = configRepository;
        }

		public static IEnumerable<T> GetEntities<T>(Stream inputStream, string fileName)
        {
            List<T> entities;
            using (var sr = new StreamReader(inputStream))
            {

                var t = Path.GetExtension(fileName);
                if (Path.GetExtension(fileName).ToLower() == ".csv")
                {
                    var csv = new CsvReader(sr);
                    entities = csv.GetRecords<T>().ToList();
                }
                else
                {
                    using (var jsonTextReader = new JsonTextReader(sr))
                    {
                       var serializer = new JsonSerializer();
                       entities = serializer.Deserialize<List<T>>(jsonTextReader);
                    }
                }
            }
            return entities;
        }

        public IEnumerable<string> GetUnlinkedMediaKeys()
        {
            var uploadedFiles = _fileStorageService.AllKeys().ToList();
            foreach(var item in _employeeRepository.Get())
            {
                ExcludeFromList(uploadedFiles, item.ImgMedia?.Id);
                ExcludeFromList(uploadedFiles, item.EmployeeFileMedia?.Id);
                foreach(var file in item.EmployeeGalleryMediaList?.Select(x => x.Id))
                {
                    ExcludeFromList(uploadedFiles, file);
                }
                foreach(var file in item.EmployeeFileListMediaList?.Select(x => x.Id))
                {
                    ExcludeFromList(uploadedFiles, file);
                }
            }
            foreach(var item in _projectRepository.Get())
            {
                foreach(var file in item.ProjectGalleryMediaList?.Select(x => x.Id))
                {
                    ExcludeFromList(uploadedFiles, file);
                }
            }
            return uploadedFiles;
        }

        public void SetIdentities()
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var roleEntities = _roleRepository.Get().Where(e => e.Identity == default(Guid)).ToList();
                foreach (var entity in roleEntities)
                {
                    entity.Identity = Guid.NewGuid();
                }
                var userEntities = _userRepository.Get().Where(e => e.Identity == default(Guid)).ToList();
                foreach (var entity in userEntities)
                {
                    entity.Identity = Guid.NewGuid();
                }
                var employeeEntities = _employeeRepository.Get().Where(e => e.Identity == default(Guid)).ToList();
                foreach (var entity in employeeEntities)
                {
                    entity.Identity = Guid.NewGuid();
                }
                var projectEntities = _projectRepository.Get().Where(e => e.Identity == default(Guid)).ToList();
                foreach (var entity in projectEntities)
                {
                    entity.Identity = Guid.NewGuid();
                }
                var configEntities = _configRepository.Get().Where(e => e.Identity == default(Guid)).ToList();
                foreach (var entity in configEntities)
                {
                    entity.Identity = Guid.NewGuid();
                }
                scope.SaveChanges();
            }
        }

        private void ExcludeFromList(IList<string> list, string value) 
        {
            if (string.IsNullOrWhiteSpace(value) || value == "[]" || value == "{}")
                return;
            list.Remove(value);
        }


    }
}