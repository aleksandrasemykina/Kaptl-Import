/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Web;
namespace Web.Services
{
    public class HttpContextService : IHttpContextService
    {
        public string GetCookie(string name)
        {
            if (HttpContext.Current == null)
                return null;

            var httpCookie = HttpContext.Current.Request.Cookies[name];
            return httpCookie?.Values[0];
        }

        public void SetCookie(string name, string value, bool addExpires = false)
        {
            if (HttpContext.Current == null)
                return;

            var cookie = new HttpCookie(name, value);
            if (addExpires)
                cookie.Expires = DateTime.MaxValue;
            HttpContext.Current.Response.SetCookie(cookie);
        }

        public void DeleteCookie(string name)
        {
            if (HttpContext.Current == null)
                return;

            HttpContext.Current.Response.Cookies.Remove(name);
            var cookie = new HttpCookie(name, string.Empty)
            {
                Expires = DateTime.MinValue
            };
            HttpContext.Current.Response.SetCookie(cookie);
        }
    }
}
