﻿var gulp = require("gulp");
var bower = require("gulp-bower");
var sass = require("gulp-sass");
var $ = require('gulp-load-plugins')();

var adminConfig = require('./gulp-admin.config');

module.exports = function () {

    gulp.task("watch-admin", function () {
        gulp.watch(adminConfig.contentFolder + "/css/*.scss", ["css-admin"]);
        gulp.watch([adminConfig.contentFolder + '/js/**/*.html'], ['javascript-admin']);
        gulp.watch(adminConfig.js, ['javascript-admin']);
    });

    gulp.task("bower-admin", function () {
        return bower({ directory: "./bower_components", cwd: adminConfig.contentFolder })
            .pipe(gulp.dest(adminConfig.contentFolder + "/bower_components"));
    });

    gulp.task('javascript-admin', ['angularTemplates-admin'], function () {
        var jsPipe = gulp.src(adminConfig.js)
            .pipe($.plumber())
            .pipe($.if(adminConfig.debug, $.sourcemaps.init()))
            .pipe($.concat('app.js'))
            .pipe($.if(!adminConfig.debug, $.uglify()))
            .pipe($.if(adminConfig.debug, $.sourcemaps.write()))
            .pipe(gulp.dest(adminConfig.contentFolder));
        return jsPipe;
    });

    gulp.task('angularTemplates-admin', function () {
        return gulp.src(adminConfig.contentFolder + '/js/admin-app/**/*.html')
            .pipe($.plumber())
            .pipe($.angularTemplatecache({
                module: 'app-templates',
                root: '/areas/kaptl/Content/js/admin-app'
            }))
            .pipe(gulp.dest(adminConfig.contentFolder + '/js/admin-app'));
    });

    gulp.task('bowerJs-admin', function () {
        var jsPipe = gulp.src(adminConfig.bowerJs())
            .pipe($.plumber())
            .pipe($.if(adminConfig.debug, $.sourcemaps.init()))
            .pipe($.concat('vendor.js'))
            .pipe($.if(!adminConfig.debug, $.uglify()))
            .pipe($.if(adminConfig.debug, $.sourcemaps.write()))
            .pipe(gulp.dest(adminConfig.contentFolder));
        return jsPipe;
    });

    gulp.task('bowerCss-admin', ['fonts-admin'], function () {
        var cssPipe = gulp.src(adminConfig.bowerCss())
            .pipe($.plumber())
            .pipe($.concat('vendor.css'))
            .pipe($.if(!adminConfig.debug, $.cleanCss()))
            .pipe(gulp.dest(adminConfig.contentFolder + "/css"));
        return cssPipe;
    });

    gulp.task('fonts-admin', function () {
        return gulp.src([
            adminConfig.bowerFolder + '/font-awesome/fonts/**.*',
            adminConfig.bowerFolder + '/bootstrap/dist/fonts/**.*'])
            .pipe(gulp.dest(adminConfig.contentFolder + '/fonts'));
    });

    gulp.task("css-admin", function () {
        return gulp.src(adminConfig.contentFolder + "/scss/*.scss")
            .pipe($.plumber())
            .pipe($.if(adminConfig.debug, $.sourcemaps.init()))
            .pipe(sass({ outputStyle: 'compressed' }).on("error", sass.logError))
            .pipe($.concat('styles.css'))
            .pipe($.if(adminConfig.debug, $.sourcemaps.write()))
            .pipe(gulp.dest(adminConfig.contentFolder + "/css"));
    });

    gulp.task('build-admin', ['bowerCss-admin', 'bowerJs-admin', 'css-admin', 'javascript-admin']);

};