﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Core.Web.Controllers;
using Core.Web.Infrastructure;
using log4net;

namespace Web
{
    public class MvcApplication : HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();
            DbContextConfig.Configure();

            AutoMapperConfig.Configure(new [] {typeof(BaseController).Assembly, typeof(AutoMapperConfig).Assembly});
            UnityConfig.Configure();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            UnityConfig.RegisterTypes();

            ModelBinderConfig.RegisterModelBinders();

			ControllerBuilder.Current.SetControllerFactory(new GlobalControllerFactory());

            AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
            GlobalConfiguration
            .Configuration
            .Formatters
            .JsonFormatter
            .SerializerSettings
            .DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Populate;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Log.Error("Unhandled exception.", Server.GetLastError());
        }
    }
}