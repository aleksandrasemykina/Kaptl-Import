var gulp = require("gulp");
var bower = require("gulp-bower");
var $ = require('gulp-load-plugins')();

try {
    var config = require('./gulp.config');
    var admin = require('./gulp-admin.js')();

    gulp.task("bower", function () {
        return bower({ directory: "./bower_components", cwd: "./Content" })
            .pipe(gulp.dest(config.contentFolder + "/bower_components"));
    });

    gulp.task('javascript', function () {
        var jsPipe = gulp.src(config.js)
            .pipe($.plumber())
            .pipe($.if(config.debug, $.sourcemaps.init()))
            .pipe($.concat('app.js'))
            .pipe($.if(!config.debug, $.uglify()))
            .pipe($.if(config.debug, $.sourcemaps.write()))
            .pipe(gulp.dest(config.contentFolder));
        return jsPipe;
    });

    gulp.task('bowerJs', function () {
        var jsPipe = gulp.src(config.bowerJs())
            .pipe($.plumber())
            .pipe($.if(config.debug, $.sourcemaps.init()))
            .pipe($.concat('vendor.js'))
            .pipe($.if(!config.debug, $.uglify()))
            .pipe($.if(config.debug, $.sourcemaps.write()))
            .pipe(gulp.dest(config.contentFolder));
        return jsPipe;
    });

    gulp.task('fonts', function () {
        return gulp.src([
            config.bowerFolder + '/font-awesome/fonts/**.*',
            config.bowerFolder + '/bootstrap/dist/fonts/**.*'])
            .pipe(gulp.dest(config.contentFolder + '/fonts'));
    });

    gulp.task('bowerCss', ["fonts"], function() {
        var cssPipe = gulp.src(config.bowerCss())
            .pipe($.plumber())
            .pipe($.concatCss('vendor.css', { rebaseUrls: false }))
            .pipe($.if(!config.debug, $.cleanCss()))
            .pipe(gulp.dest(config.contentFolder + "/css"));
        return cssPipe;
    });

    gulp.task("css", function () {
        return gulp.src(config.contentFolder + "/scss/*.scss")
            .pipe($.plumber())
            .pipe($.if(config.debug, $.sourcemaps.init()))
            .pipe($.sass({ outputStyle: 'compressed' }).on("error", $.sass.logError))
            .pipe($.concat('styles.css'))
            .pipe($.if(config.debug, $.sourcemaps.write()))
            .pipe(gulp.dest(config.contentFolder + "/css"));
    });

    gulp.task('build', ['bowerCss', 'bowerJs', 'css', 'javascript']);

    //*******************************************************************************************************************************

    gulp.task('_build-all', ['build-admin', 'build']);

    gulp.task("watch-app", function () {
        gulp.watch(config.contentFolder + "/scss/*.scss", ["css"]);        
        gulp.watch([config.contentFolder + '/js/**/*.html'], ['javascript']);
        gulp.watch(config.js, ['javascript']);
    });

    gulp.task("_watch", ["watch-app", "watch-admin"]);

} catch (e) {
    console.log("Looks like some bower components are not installed. Run 'bower-force' task. ", e);
    gulp.task("bower", function () {
        return bower({ directory: "./bower_components", cwd: "./Content", force: true })
            .pipe(gulp.dest(config.contentFolder + "/bower_components"));
    });

    gulp.task("bower-admin", function () {
        return bower({ directory: "./bower_components", cwd: "./Areas/kaptl/Content", force: true })
            .pipe(gulp.dest("./Areas/kaptl/Content/bower_components"));
    });

    gulp.task('build-force', ['bower', 'bower-admin']);
}