﻿using System;
using System.Web;
using System.Web.WebPages;
using NonFactors.Mvc.Grid;


namespace Web.Infrastructure
{
    public static class MvcGridExtensions
    {
        public static IGridColumn<T> RenderedAs<T>(this IGridColumn<T> column, Func<T, IHtmlString> constraint)
        {
            Func<T, string> func = (Func<T, string>)(a => constraint(a).ToHtmlString());
            return column.RenderedAs((Func<T, object>)func);
        }

        public static IGridColumn<T> RenderedAs<T>(this IGridColumn<T> column, Func<T, Func<object, HelperResult>> constraint)
        {
            Func<T, string> func = (Func<T, string>)(a => constraint(a)((object)null).ToHtmlString());
            return column.RenderedAs((Func<T, object>)func);
        }
    }
}