﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows.Forms;
using Core.Services;
using Microsoft.Practices.Unity;

namespace Web.Infrastructure
{
    public class Kaptl
    {
        public static IConfigService Config
        {
            get { return UnityConfig.Container.Resolve<IConfigService>(); }
        }
    }
}