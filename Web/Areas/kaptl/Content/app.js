(function () {
    'use strict';

    angular.module('app-templates', [
    ]);

})();

(function () {
    'use strict';

    angular.module('file-upload-field', [
        'ngFileUpload'
    ]);

})();

angular.module("app-templates").run(["$templateCache", function($templateCache) {$templateCache.put("/areas/kaptl/Content/js/admin-app/file-upload-field/file-upload-field.html","<div>\r\n    <a href=\"\" ngf-select=\"vm.uploadFiles($files, $invalidFiles)\" ngf-multiple=\"vm.isMultiple\">Add</a>\r\n    <ul>\r\n        <li ng-if=\"vm.uploads.length === 0\">No files uploaded.</li>\r\n        <li ng-repeat=\"file in vm.uploads\">\r\n            <div ng-if=\"file.isImage && vm.showImagePreview\">\r\n                <a ng-href=\"{{file.publicUrl}}\" target=\"_blank\"><img ng-src=\"{{file.publicUrl}}\" width=\"100\" alt=\"{{file.name}}\" /></a>\r\n                <a href=\"\" ng-click=\"vm.removeUpload(file.id)\">Remove</a>\r\n            </div>\r\n            <div ng-if=\"!file.isImage || !vm.showImagePreview\">\r\n                <a ng-href=\"{{file.publicUrl}}\" target=\"_blank\">{{file.name}}</a>\r\n                <a href=\"\" ng-click=\"vm.removeUpload(file.id)\">Remove</a>\r\n            </div>\r\n        </li>\r\n    </ul>\r\n    <input type=\"hidden\" name=\"{{vm.fieldName}}\" value=\"{{vm.uploads | json}}\" />\r\n</div>\r\n");}]);
(function () {
    'use strict';

    angular.module('file-upload-field')
        .directive('fileUploadField', UploadFieldDirective);

    UploadFieldDirective.$inject = [];

    function UploadFieldDirective() {
        var directive = {
            restrict: 'E',
            scope: {},
            bindToController: {
                fieldValue: '@',
                fieldName: '@',
                uploadFolder: '@',
                isMultiple: '=',
                showImagePreview: '='
            },
            controller: UploadFieldController,
            controllerAs: 'vm',
            templateUrl: '/Areas/kaptl/Content/js/admin-app/file-upload-field/file-upload-field.html'
        };
        return directive;
    }

    UploadFieldController.$inject = ['$http', 'Upload', '$timeout'];

    function UploadFieldController($http, Upload, $timeout) {
        var vm = this;

        vm.uploadFiles = uploadFiles;
        vm.removeUpload = removeUpload;
        vm.uploads = [];

        init();

        function init() {
            if (vm.fieldValue) {
                var uploads = JSON.parse(vm.fieldValue);
                for (var i = 0; i < uploads.length; i++) {
                    addUpload(uploads[i]);
                }
            }
        }

        function addUpload(file) {
            file.isImage = /^image/.test(file.contentType);
            if (!vm.isMultiple) {
                vm.uploads = [];
            }
            vm.uploads.push(file);
        }

        function removeUpload(fileId) {
            var index = -1;
            for (var i = 0; i < vm.uploads.length; i++) {
                if (vm.uploads[i].id === fileId) {
                    index = i;
                    break;
                }
            }
            if (index !== -1) {
                vm.uploads.splice(index, 1);
            }
        }

        function uploadFiles(files, errFiles) {

            vm.uploadFilesList = files;
            vm.uploadErrorFilesList = errFiles;

            angular.forEach(files, function (file) {
                file.upload = Upload.upload({
                    url: '/kaptl/api/upload-file',
                    data: { file: file }
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        if (response.data) {
                            angular.forEach(response.data, function (fileId) {
                                addUpload(fileId);
                            });
                        }

                        file.result = response.data;
                    });
                }, function (response) {
                    /*if (response.status > 0)
                        console.log(response.status + ': ' + response.data);*/
                }, function (evt) {
                    /*
                    file.progress = Math.min(100, parseInt(100.0 *
                                             evt.loaded / evt.total));*/
                });
            });
        }
    }
})();
(function($) {
    "use strict";

    $.blockUI.defaults.message = "<i class=\"fa fa-spinner fa-spin fa-4x\"></i>";

    $.blockUI.defaults.css = {
        padding: 0,
        margin: 0,
        width: "30%",
        top: "40%",
        left: "35%",
        textAlign: "center",
        color: "#fff",
        border: "none",
        backgroundColor: "transparent",
        cursor: "wait"
    };

    $.ajaxPrefilter(function(options) {
        var token = $("input[name^=__RequestVerificationToken]").val();
        options.headers = { RequestVerificationToken: token };
    });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    window.onbeforeunload = function() {
        $.blockUI();
    };

    window.App = {
        go: function(url) {
            window.location = url;
        },

        initPage: function() {
            if ($.fn.datepicker)
                $("input.js-datepicker").datepicker({
                    todayBtn: "linked",
                    clearBtn: true,
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true
                });

            if ($.fn.mvcgrid)
                $(".mvc-grid").mvcgrid();
        },

        confirmDelete: function(entity) {
            if (!entity)
                entity = "record";
            return confirm("Do you wish to delete the " + entity + "?");
        }
    };
    App.initPage();

    tinymce.baseURL = "/areas/kaptl/content/components/tinymce-dist";
    tinymce.init({
        selector: 'textarea.richtext'
    });

    

})(jQuery);
(function () {
    'use strict';

    angular.module('app', [
        // Angular modules
        'ngRoute',
        // Custom modules

        // 3rd Party Modules
        'app-templates',
        'file-upload-field'
    ]);
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkbWluLWFwcC90ZW1wbGF0ZXMubW9kdWxlLmpzIiwiYWRtaW4tYXBwL2ZpbGUtdXBsb2FkLWZpZWxkL2ZpbGUtdXBsb2FkLWZpZWxkLm1vZHVsZS5qcyIsImFkbWluLWFwcC90ZW1wbGF0ZXMuanMiLCJhZG1pbi1hcHAvZmlsZS11cGxvYWQtZmllbGQvZmlsZS11cGxvYWQtZmllbGQuZGlyZWN0aXZlLmpzIiwiZGVmYXVsdC5qcyIsImFkbWluLWFwcC9hcHAuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1JBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbEdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDL0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uICgpIHtcclxuICAgICd1c2Ugc3RyaWN0JztcclxuXHJcbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLXRlbXBsYXRlcycsIFtcclxuICAgIF0pO1xyXG5cclxufSkoKTtcclxuIiwiKGZ1bmN0aW9uICgpIHtcclxuICAgICd1c2Ugc3RyaWN0JztcclxuXHJcbiAgICBhbmd1bGFyLm1vZHVsZSgnZmlsZS11cGxvYWQtZmllbGQnLCBbXHJcbiAgICAgICAgJ25nRmlsZVVwbG9hZCdcclxuICAgIF0pO1xyXG5cclxufSkoKTtcclxuIiwiYW5ndWxhci5tb2R1bGUoXCJhcHAtdGVtcGxhdGVzXCIpLnJ1bihbXCIkdGVtcGxhdGVDYWNoZVwiLCBmdW5jdGlvbigkdGVtcGxhdGVDYWNoZSkgeyR0ZW1wbGF0ZUNhY2hlLnB1dChcIi9hcmVhcy9rYXB0bC9Db250ZW50L2pzL2FkbWluLWFwcC9maWxlLXVwbG9hZC1maWVsZC9maWxlLXVwbG9hZC1maWVsZC5odG1sXCIsXCI8ZGl2PlxcclxcbiAgICA8YSBocmVmPVxcXCJcXFwiIG5nZi1zZWxlY3Q9XFxcInZtLnVwbG9hZEZpbGVzKCRmaWxlcywgJGludmFsaWRGaWxlcylcXFwiIG5nZi1tdWx0aXBsZT1cXFwidm0uaXNNdWx0aXBsZVxcXCI+QWRkPC9hPlxcclxcbiAgICA8dWw+XFxyXFxuICAgICAgICA8bGkgbmctaWY9XFxcInZtLnVwbG9hZHMubGVuZ3RoID09PSAwXFxcIj5ObyBmaWxlcyB1cGxvYWRlZC48L2xpPlxcclxcbiAgICAgICAgPGxpIG5nLXJlcGVhdD1cXFwiZmlsZSBpbiB2bS51cGxvYWRzXFxcIj5cXHJcXG4gICAgICAgICAgICA8ZGl2IG5nLWlmPVxcXCJmaWxlLmlzSW1hZ2UgJiYgdm0uc2hvd0ltYWdlUHJldmlld1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDxhIG5nLWhyZWY9XFxcInt7ZmlsZS5wdWJsaWNVcmx9fVxcXCIgdGFyZ2V0PVxcXCJfYmxhbmtcXFwiPjxpbWcgbmctc3JjPVxcXCJ7e2ZpbGUucHVibGljVXJsfX1cXFwiIHdpZHRoPVxcXCIxMDBcXFwiIGFsdD1cXFwie3tmaWxlLm5hbWV9fVxcXCIgLz48L2E+XFxyXFxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XFxcIlxcXCIgbmctY2xpY2s9XFxcInZtLnJlbW92ZVVwbG9hZChmaWxlLmlkKVxcXCI+UmVtb3ZlPC9hPlxcclxcbiAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgIDxkaXYgbmctaWY9XFxcIiFmaWxlLmlzSW1hZ2UgfHwgIXZtLnNob3dJbWFnZVByZXZpZXdcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8YSBuZy1ocmVmPVxcXCJ7e2ZpbGUucHVibGljVXJsfX1cXFwiIHRhcmdldD1cXFwiX2JsYW5rXFxcIj57e2ZpbGUubmFtZX19PC9hPlxcclxcbiAgICAgICAgICAgICAgICA8YSBocmVmPVxcXCJcXFwiIG5nLWNsaWNrPVxcXCJ2bS5yZW1vdmVVcGxvYWQoZmlsZS5pZClcXFwiPlJlbW92ZTwvYT5cXHJcXG4gICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgIDwvbGk+XFxyXFxuICAgIDwvdWw+XFxyXFxuICAgIDxpbnB1dCB0eXBlPVxcXCJoaWRkZW5cXFwiIG5hbWU9XFxcInt7dm0uZmllbGROYW1lfX1cXFwiIHZhbHVlPVxcXCJ7e3ZtLnVwbG9hZHMgfCBqc29ufX1cXFwiIC8+XFxyXFxuPC9kaXY+XFxyXFxuXCIpO31dKTsiLCIoZnVuY3Rpb24gKCkge1xyXG4gICAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICAgIGFuZ3VsYXIubW9kdWxlKCdmaWxlLXVwbG9hZC1maWVsZCcpXHJcbiAgICAgICAgLmRpcmVjdGl2ZSgnZmlsZVVwbG9hZEZpZWxkJywgVXBsb2FkRmllbGREaXJlY3RpdmUpO1xyXG5cclxuICAgIFVwbG9hZEZpZWxkRGlyZWN0aXZlLiRpbmplY3QgPSBbXTtcclxuXHJcbiAgICBmdW5jdGlvbiBVcGxvYWRGaWVsZERpcmVjdGl2ZSgpIHtcclxuICAgICAgICB2YXIgZGlyZWN0aXZlID0ge1xyXG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxyXG4gICAgICAgICAgICBzY29wZToge30sXHJcbiAgICAgICAgICAgIGJpbmRUb0NvbnRyb2xsZXI6IHtcclxuICAgICAgICAgICAgICAgIGZpZWxkVmFsdWU6ICdAJyxcclxuICAgICAgICAgICAgICAgIGZpZWxkTmFtZTogJ0AnLFxyXG4gICAgICAgICAgICAgICAgdXBsb2FkRm9sZGVyOiAnQCcsXHJcbiAgICAgICAgICAgICAgICBpc011bHRpcGxlOiAnPScsXHJcbiAgICAgICAgICAgICAgICBzaG93SW1hZ2VQcmV2aWV3OiAnPSdcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY29udHJvbGxlcjogVXBsb2FkRmllbGRDb250cm9sbGVyLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyQXM6ICd2bScsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL0FyZWFzL2thcHRsL0NvbnRlbnQvanMvYWRtaW4tYXBwL2ZpbGUtdXBsb2FkLWZpZWxkL2ZpbGUtdXBsb2FkLWZpZWxkLmh0bWwnXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gZGlyZWN0aXZlO1xyXG4gICAgfVxyXG5cclxuICAgIFVwbG9hZEZpZWxkQ29udHJvbGxlci4kaW5qZWN0ID0gWyckaHR0cCcsICdVcGxvYWQnLCAnJHRpbWVvdXQnXTtcclxuXHJcbiAgICBmdW5jdGlvbiBVcGxvYWRGaWVsZENvbnRyb2xsZXIoJGh0dHAsIFVwbG9hZCwgJHRpbWVvdXQpIHtcclxuICAgICAgICB2YXIgdm0gPSB0aGlzO1xyXG5cclxuICAgICAgICB2bS51cGxvYWRGaWxlcyA9IHVwbG9hZEZpbGVzO1xyXG4gICAgICAgIHZtLnJlbW92ZVVwbG9hZCA9IHJlbW92ZVVwbG9hZDtcclxuICAgICAgICB2bS51cGxvYWRzID0gW107XHJcblxyXG4gICAgICAgIGluaXQoKTtcclxuXHJcbiAgICAgICAgZnVuY3Rpb24gaW5pdCgpIHtcclxuICAgICAgICAgICAgaWYgKHZtLmZpZWxkVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHZhciB1cGxvYWRzID0gSlNPTi5wYXJzZSh2bS5maWVsZFZhbHVlKTtcclxuICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdXBsb2Fkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGFkZFVwbG9hZCh1cGxvYWRzW2ldKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gYWRkVXBsb2FkKGZpbGUpIHtcclxuICAgICAgICAgICAgZmlsZS5pc0ltYWdlID0gL15pbWFnZS8udGVzdChmaWxlLmNvbnRlbnRUeXBlKTtcclxuICAgICAgICAgICAgaWYgKCF2bS5pc011bHRpcGxlKSB7XHJcbiAgICAgICAgICAgICAgICB2bS51cGxvYWRzID0gW107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdm0udXBsb2Fkcy5wdXNoKGZpbGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gcmVtb3ZlVXBsb2FkKGZpbGVJZCkge1xyXG4gICAgICAgICAgICB2YXIgaW5kZXggPSAtMTtcclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB2bS51cGxvYWRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodm0udXBsb2Fkc1tpXS5pZCA9PT0gZmlsZUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaW5kZXggPSBpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChpbmRleCAhPT0gLTEpIHtcclxuICAgICAgICAgICAgICAgIHZtLnVwbG9hZHMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gdXBsb2FkRmlsZXMoZmlsZXMsIGVyckZpbGVzKSB7XHJcblxyXG4gICAgICAgICAgICB2bS51cGxvYWRGaWxlc0xpc3QgPSBmaWxlcztcclxuICAgICAgICAgICAgdm0udXBsb2FkRXJyb3JGaWxlc0xpc3QgPSBlcnJGaWxlcztcclxuXHJcbiAgICAgICAgICAgIGFuZ3VsYXIuZm9yRWFjaChmaWxlcywgZnVuY3Rpb24gKGZpbGUpIHtcclxuICAgICAgICAgICAgICAgIGZpbGUudXBsb2FkID0gVXBsb2FkLnVwbG9hZCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiAnL2thcHRsL2FwaS91cGxvYWQtZmlsZScsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogeyBmaWxlOiBmaWxlIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgZmlsZS51cGxvYWQudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAkdGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmZvckVhY2gocmVzcG9uc2UuZGF0YSwgZnVuY3Rpb24gKGZpbGVJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZFVwbG9hZChmaWxlSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGUucmVzdWx0ID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8qaWYgKHJlc3BvbnNlLnN0YXR1cyA+IDApXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLnN0YXR1cyArICc6ICcgKyByZXNwb25zZS5kYXRhKTsqL1xyXG4gICAgICAgICAgICAgICAgfSwgZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICAgICAgZmlsZS5wcm9ncmVzcyA9IE1hdGgubWluKDEwMCwgcGFyc2VJbnQoMTAwLjAgKlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldnQubG9hZGVkIC8gZXZ0LnRvdGFsKSk7Ki9cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pKCk7IiwiKGZ1bmN0aW9uKCQpIHtcclxuICAgIFwidXNlIHN0cmljdFwiO1xyXG5cclxuICAgICQuYmxvY2tVSS5kZWZhdWx0cy5tZXNzYWdlID0gXCI8aSBjbGFzcz1cXFwiZmEgZmEtc3Bpbm5lciBmYS1zcGluIGZhLTR4XFxcIj48L2k+XCI7XHJcblxyXG4gICAgJC5ibG9ja1VJLmRlZmF1bHRzLmNzcyA9IHtcclxuICAgICAgICBwYWRkaW5nOiAwLFxyXG4gICAgICAgIG1hcmdpbjogMCxcclxuICAgICAgICB3aWR0aDogXCIzMCVcIixcclxuICAgICAgICB0b3A6IFwiNDAlXCIsXHJcbiAgICAgICAgbGVmdDogXCIzNSVcIixcclxuICAgICAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXHJcbiAgICAgICAgY29sb3I6IFwiI2ZmZlwiLFxyXG4gICAgICAgIGJvcmRlcjogXCJub25lXCIsXHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcInRyYW5zcGFyZW50XCIsXHJcbiAgICAgICAgY3Vyc29yOiBcIndhaXRcIlxyXG4gICAgfTtcclxuXHJcbiAgICAkLmFqYXhQcmVmaWx0ZXIoZnVuY3Rpb24ob3B0aW9ucykge1xyXG4gICAgICAgIHZhciB0b2tlbiA9ICQoXCJpbnB1dFtuYW1lXj1fX1JlcXVlc3RWZXJpZmljYXRpb25Ub2tlbl1cIikudmFsKCk7XHJcbiAgICAgICAgb3B0aW9ucy5oZWFkZXJzID0geyBSZXF1ZXN0VmVyaWZpY2F0aW9uVG9rZW46IHRva2VuIH07XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKGRvY3VtZW50KS5hamF4U3RhcnQoJC5ibG9ja1VJKS5hamF4U3RvcCgkLnVuYmxvY2tVSSk7XHJcblxyXG4gICAgd2luZG93Lm9uYmVmb3JldW5sb2FkID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJC5ibG9ja1VJKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIHdpbmRvdy5BcHAgPSB7XHJcbiAgICAgICAgZ286IGZ1bmN0aW9uKHVybCkge1xyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24gPSB1cmw7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaW5pdFBhZ2U6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBpZiAoJC5mbi5kYXRlcGlja2VyKVxyXG4gICAgICAgICAgICAgICAgJChcImlucHV0LmpzLWRhdGVwaWNrZXJcIikuZGF0ZXBpY2tlcih7XHJcbiAgICAgICAgICAgICAgICAgICAgdG9kYXlCdG46IFwibGlua2VkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJCdG46IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FsZW5kYXJXZWVrczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBhdXRvY2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgdG9kYXlIaWdobGlnaHQ6IHRydWVcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKCQuZm4ubXZjZ3JpZClcclxuICAgICAgICAgICAgICAgICQoXCIubXZjLWdyaWRcIikubXZjZ3JpZCgpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNvbmZpcm1EZWxldGU6IGZ1bmN0aW9uKGVudGl0eSkge1xyXG4gICAgICAgICAgICBpZiAoIWVudGl0eSlcclxuICAgICAgICAgICAgICAgIGVudGl0eSA9IFwicmVjb3JkXCI7XHJcbiAgICAgICAgICAgIHJldHVybiBjb25maXJtKFwiRG8geW91IHdpc2ggdG8gZGVsZXRlIHRoZSBcIiArIGVudGl0eSArIFwiP1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgQXBwLmluaXRQYWdlKCk7XHJcblxyXG4gICAgdGlueW1jZS5iYXNlVVJMID0gXCIvYXJlYXMva2FwdGwvY29udGVudC9jb21wb25lbnRzL3RpbnltY2UtZGlzdFwiO1xyXG4gICAgdGlueW1jZS5pbml0KHtcclxuICAgICAgICBzZWxlY3RvcjogJ3RleHRhcmVhLnJpY2h0ZXh0J1xyXG4gICAgfSk7XHJcblxyXG4gICAgXHJcblxyXG59KShqUXVlcnkpOyIsIihmdW5jdGlvbiAoKSB7XHJcbiAgICAndXNlIHN0cmljdCc7XHJcblxyXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcCcsIFtcclxuICAgICAgICAvLyBBbmd1bGFyIG1vZHVsZXNcclxuICAgICAgICAnbmdSb3V0ZScsXHJcbiAgICAgICAgLy8gQ3VzdG9tIG1vZHVsZXNcclxuXHJcbiAgICAgICAgLy8gM3JkIFBhcnR5IE1vZHVsZXNcclxuICAgICAgICAnYXBwLXRlbXBsYXRlcycsXHJcbiAgICAgICAgJ2ZpbGUtdXBsb2FkLWZpZWxkJ1xyXG4gICAgXSk7XHJcbn0pKCk7XHJcbiJdfQ==
