﻿(function () {
    'use strict';

    angular.module('file-upload-field')
        .directive('fileUploadField', UploadFieldDirective);

    UploadFieldDirective.$inject = [];

    function UploadFieldDirective() {
        var directive = {
            restrict: 'E',
            scope: {},
            bindToController: {
                fieldValue: '@',
                fieldName: '@',
                uploadFolder: '@',
                isMultiple: '=',
                showImagePreview: '='
            },
            controller: UploadFieldController,
            controllerAs: 'vm',
            templateUrl: '/Areas/kaptl/Content/js/admin-app/file-upload-field/file-upload-field.html'
        };
        return directive;
    }

    UploadFieldController.$inject = ['$http', 'Upload', '$timeout'];

    function UploadFieldController($http, Upload, $timeout) {
        var vm = this;

        vm.uploadFiles = uploadFiles;
        vm.removeUpload = removeUpload;
        vm.uploads = [];

        init();

        function init() {
            if (vm.fieldValue) {
                var uploads = JSON.parse(vm.fieldValue);
                for (var i = 0; i < uploads.length; i++) {
                    addUpload(uploads[i]);
                }
            }
        }

        function addUpload(file) {
            file.isImage = /^image/.test(file.contentType);
            if (!vm.isMultiple) {
                vm.uploads = [];
            }
            vm.uploads.push(file);
        }

        function removeUpload(fileId) {
            var index = -1;
            for (var i = 0; i < vm.uploads.length; i++) {
                if (vm.uploads[i].id === fileId) {
                    index = i;
                    break;
                }
            }
            if (index !== -1) {
                vm.uploads.splice(index, 1);
            }
        }

        function uploadFiles(files, errFiles) {

            vm.uploadFilesList = files;
            vm.uploadErrorFilesList = errFiles;

            angular.forEach(files, function (file) {
                file.upload = Upload.upload({
                    url: '/kaptl/api/upload-file',
                    data: { file: file }
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        if (response.data) {
                            angular.forEach(response.data, function (fileId) {
                                addUpload(fileId);
                            });
                        }

                        file.result = response.data;
                    });
                }, function (response) {
                    /*if (response.status > 0)
                        console.log(response.status + ': ' + response.data);*/
                }, function (evt) {
                    /*
                    file.progress = Math.min(100, parseInt(100.0 *
                                             evt.loaded / evt.total));*/
                });
            });
        }
    }
})();