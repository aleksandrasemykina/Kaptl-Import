﻿(function () {
    'use strict';

    angular.module('file-upload-field', [
        'ngFileUpload'
    ]);

})();
