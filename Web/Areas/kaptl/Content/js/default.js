(function($) {
    "use strict";

    $.blockUI.defaults.message = "<i class=\"fa fa-spinner fa-spin fa-4x\"></i>";

    $.blockUI.defaults.css = {
        padding: 0,
        margin: 0,
        width: "30%",
        top: "40%",
        left: "35%",
        textAlign: "center",
        color: "#fff",
        border: "none",
        backgroundColor: "transparent",
        cursor: "wait"
    };

    $.ajaxPrefilter(function(options) {
        var token = $("input[name^=__RequestVerificationToken]").val();
        options.headers = { RequestVerificationToken: token };
    });

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    window.onbeforeunload = function() {
        $.blockUI();
    };

    window.App = {
        go: function(url) {
            window.location = url;
        },

        initPage: function() {
            if ($.fn.datepicker)
                $("input.js-datepicker").datepicker({
                    todayBtn: "linked",
                    clearBtn: true,
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true
                });

            if ($.fn.mvcgrid)
                $(".mvc-grid").mvcgrid();
        },

        confirmDelete: function(entity) {
            if (!entity)
                entity = "record";
            return confirm("Do you wish to delete the " + entity + "?");
        }
    };
    App.initPage();

    tinymce.baseURL = "/areas/kaptl/content/components/tinymce-dist";
    tinymce.init({
        selector: 'textarea.richtext'
    });

    

})(jQuery);