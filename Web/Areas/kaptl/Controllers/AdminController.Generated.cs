﻿
using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using Core.DataAccess;
using Core.Services;
using Core.Web.Models;
using Core.Web.Services;
using Mehdime.Entity;
using Web.Identity;
using Web.Infrastructure;
using Core.Web.Infrastructure.Mvc;
using Web.Services;

namespace Web.Areas.kaptl.Controllers
{
    public partial class AdminController : BaseAdminController
    {
        protected readonly IContentUtilsService _contentUtilsService;
        protected readonly IFileStorageService _fileStorageService;

        public AdminController(
            ApplicationRoleManager roleManager,
            ApplicationUserManager userManager,
            IDbContextScopeFactory dbContextScopeFactory,
            IUploadService uploadService,
			IConfigService configService,
            IFileStorageService fileStorageService,
            IContentUtilsService contentUtilsService,

            IRoleRepository roleRepository,
            IUserRepository userRepository,
            IEmployeeRepository employeeRepository,
            IProjectRepository projectRepository,
            IConfigRepository configRepository,
            IMapper mapper)
            : base(
                roleManager, 
                userManager, 
                dbContextScopeFactory,
                uploadService,
				configService,
                roleRepository, 
                userRepository, 
                employeeRepository, 
                projectRepository, 
                configRepository, 
                mapper)
        {
            _fileStorageService = fileStorageService;
            _contentUtilsService = contentUtilsService;
        }

        [ReadOnlyDbScope]
        public async Task<ActionResult> UnlinkedFiles()
        {
            var unlinkedFiles = _contentUtilsService.GetUnlinkedMediaKeys();
            return View(unlinkedFiles);
        }

        [ReadOnlyDbScope]
        public async Task<ActionResult> DeleteUnlinkedFiles()
        {
            var unlinkedFiles = _contentUtilsService.GetUnlinkedMediaKeys();
            foreach(var file in unlinkedFiles)
            {
                _fileStorageService.Delete(file);
            }
            return View(unlinkedFiles);
        }
    }
}