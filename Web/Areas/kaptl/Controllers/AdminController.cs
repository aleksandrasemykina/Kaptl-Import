﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using Core.Data.DomainModel;
using Core.Web;
using Core.Web.Filters;
using Core.Web.Models;
using Microsoft.AspNet.Identity;

namespace Web.Areas.kaptl.Controllers
{
    public partial class AdminController
    {
        public ActionResult Index()
        {
            return View();
        }

        private static void CheckIdentityResult(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new UserErrorException(result.Errors.FirstOrDefault());
            }
        }

        [NoCache]
        public override ActionResult _UserList()
        {
            var query = _userManager.Users.OrderBy(user => user.UserName).Select(x => new UserListItemViewModel
            {
                Id = x.Id,
                Email = x.Email,
                EmailConfirmed = x.EmailConfirmed,
                LockoutEnabled = x.LockoutEnabled,
                PhoneNumber = x.PhoneNumber,
                PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                TwoFactorEnabled = x.TwoFactorEnabled,
                UserName = x.UserName
            });
            return PartialView("_UserList.Generated", query);
        }

        public override ActionResult UserEdit(int? id, UserViewModel model)
        {
            ModelState.Clear();

            model.Roles = _roleManager.Roles.OrderBy(role => role.Name).ToList();

            if (!id.HasValue)
                return View("UserEdit.Generated", model);

            var entity = _userManager.FindById(id.Value);

            _mapper.Map(entity, model);

            foreach (var role in entity.Roles)
                model.SelectedItems.Add(model.Roles.Single(applicationRole => applicationRole.Id == role.RoleId).Name);

            return View(model);
        }

        [HttpPost]
        public override ActionResult UserEdit(UserViewModel viewModel)
        {
            viewModel.Roles = _roleManager.Roles.OrderBy(role => role.Name).ToList();

            if (ModelState.IsValid)
                try
                {
                    var entity = _userManager.FindById(viewModel.Id) ?? new User();
                    _mapper.Map(viewModel, entity);

                    if (viewModel.Id == 0)
                    {
                        if (string.IsNullOrEmpty(viewModel.Password))
                            throw new UserErrorException("Password cannot be empty.");

                        CheckIdentityResult(_userManager.Create(entity, viewModel.Password));

                        foreach (var id in viewModel.SelectedItems)
                            _userManager.AddToRole(entity.Id, id);
                    }
                    else
                    {
                        CheckIdentityResult(_userManager.Update(entity));

                        if (!string.IsNullOrEmpty(viewModel.Password))
                        {
                            var token = _userManager.GeneratePasswordResetToken(entity.Id);
                            CheckIdentityResult(_userManager.ResetPassword(entity.Id, token, viewModel.Password));
                        }

                        foreach (var role in entity.Roles.ToList())
                            _userManager.RemoveFromRole(entity.Id, viewModel.Roles.Single(applicationRole => applicationRole.Id == role.RoleId).Name);

                        foreach (var id in viewModel.SelectedItems)
                            _userManager.AddToRole(entity.Id, id);
                    }

                    AddInfoMessage("The user has been updated/created.");

                    return RedirectToReturnUrlOrAction("UserList");
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }

            return View(viewModel);
        }

        public override ActionResult UserDelete(int id)
        {
            try
            {
                var entity = _userManager.FindById(id);
                // ReSharper disable once UnusedVariable
                var result = _userManager.Delete(entity);

                AddInfoMessage("The user has been deleted.");
            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }

            return RedirectToReturnUrlOrAction("UserList");
        }

        [NoCache]
        public override ActionResult _RoleList()
        {
            var query = _roleManager.Roles.OrderBy(role => role.Name).Select(x => new RoleListItemViewModel
            {
                Id = x.Id,
                Name = x.Name
            });
            return PartialView("_RoleList.Generated", query);
        }

        public override ActionResult RoleEdit(int? id, RoleViewModel model)
        {
            ModelState.Clear();

            if (!id.HasValue)
                return View("RoleEdit.Generated", model);

            var entity = _roleManager.FindById(id.Value);

            _mapper.Map(entity, model);

            return View("RoleEdit.Generated", model);
        }

        [HttpPost]
        public override ActionResult RoleEdit(RoleViewModel viewModel)
        {
            if (ModelState.IsValid)
                try
                {
                    var entity = _roleManager.FindById(viewModel.Id) ?? new Role();
                    _mapper.Map(viewModel, entity);

                    CheckIdentityResult(viewModel.Id == 0 ? _roleManager.Create(entity) : _roleManager.Update(entity));

                    AddInfoMessage("The role has been updated/created.");

                    return RedirectToReturnUrlOrAction("RoleList");
                }
                catch (Exception ex)
                {
                    HandleException(ex, typeof(DbUpdateException));
                }

            return View("RoleEdit.Generated", viewModel);
        }

        public override ActionResult RoleDelete(int id)
        {
            try
            {
                var entity = _roleManager.FindById(id);

                if (entity.Name.Equals("admin", StringComparison.OrdinalIgnoreCase))
                {
                    throw new UserErrorException("Cannot delete built-in role. Please contact technical support.");
                }

                _roleManager.Delete(entity);

                AddInfoMessage("The role has been deleted.");
            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }

            return RedirectToReturnUrlOrAction("RoleList");
        }
    }
}
