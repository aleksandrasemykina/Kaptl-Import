

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Core.Data;
using Core.DataAccess;
using Core.Data.DomainModel;
using Core.Services;
using Core.Web.Controllers;
using Core.Web.Filters;
using Core.Web.Models;
using Core.Web.Services;
using Mehdime.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Web.Identity;
using Web.Infrastructure;
using Core.Web.Infrastructure.Mvc;
using CsvHelper;
using Web.Services;
using Web.Services.CustomSerializer;

namespace Web.Areas.kaptl.Controllers
{
    [Authorize(Roles = "admin")]
    public partial class BaseAdminController : BaseController
    {
        protected readonly ApplicationRoleManager _roleManager;
        protected readonly ApplicationUserManager _userManager;
        protected readonly IDbContextScopeFactory _dbContextScopeFactory;
        protected readonly IUploadService _uploadService;
        protected readonly IConfigService _configService;
        protected readonly IMapper _mapper;
        protected readonly IRoleRepository _roleRepository;
        protected readonly IUserRepository _userRepository;
        protected readonly IEmployeeRepository _employeeRepository;
        protected readonly IProjectRepository _projectRepository;
        protected readonly IConfigRepository _configRepository;

        public BaseAdminController(
            ApplicationRoleManager roleManager,
            ApplicationUserManager userManager,
            IDbContextScopeFactory dbContextScopeFactory,
            IUploadService uploadService,
            IConfigService configService,
            IRoleRepository roleRepository,
            IUserRepository userRepository,
            IEmployeeRepository employeeRepository,
            IProjectRepository projectRepository,
            IConfigRepository configRepository,
            IMapper mapper)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _dbContextScopeFactory = dbContextScopeFactory;
            _uploadService = uploadService;
            _configService = configService;
            _mapper = mapper;
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _employeeRepository = employeeRepository;
            _projectRepository = projectRepository;
            _configRepository = configRepository;
        }

        public virtual ActionResult RoleImport()
        {
            return View("RoleImport.Generated");
        }

        [HttpPost]
        public virtual ActionResult RoleImport(HttpPostedFileBase file)
        {
            if (file?.InputStream != null)
                try
                {
                    var entities = ContentUtilsService.GetEntities<Role>(file.InputStream, file.FileName);
                    using (var scope = _dbContextScopeFactory.Create())
                    {
                        _roleRepository.Import(entities);
                        scope.SaveChanges();
                    }
                    AddInfoMessage("The Role data has been imported.");
                    return RedirectToAction("RoleList");
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }

            return View("RoleImport.Generated");
        }

        [NoCache]
        public virtual ActionResult RoleExport()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _roleRepository.Get().ToList();

                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };

                var json = JsonConvert.SerializeObject(entities, Formatting.Indented, settings);

                return File(System.Text.Encoding.UTF8.GetBytes(json), "application/json", "RoleExport.json");
            }
        }

        [NoCache]
        public virtual ActionResult RoleExportToCsv()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _roleRepository.Get().ToList();

                using (var stream = new MemoryStream())
                using (var reader = new StreamReader(stream))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(entities);
                    writer.Flush();
                    stream.Position = 0;
                    var text = reader.ReadToEnd();
                    return File(System.Text.Encoding.UTF8.GetBytes(text), "text/csv", "RoleExport.csv");
                }
            }
        }

        [ReadOnlyDbScope]
        public virtual ActionResult RoleEdit(int? id, RoleViewModel model)
        {

            ModelState.Clear();

            if (!id.HasValue)
            {
                return View("RoleEdit.Generated", model);
            }
            var entity = _roleRepository.GetById(id.Value);

            if (entity == null)
            {
                throw new DomainException(string.Format("The role with ID = '{0}' not found.", id));
            }
            _mapper.Map(entity, model);
            return View("RoleEdit.Generated", model);
        }

        [HttpPost]
        public virtual ActionResult RoleEdit(RoleViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var scope = _dbContextScopeFactory.Create(DbContextScopeOption.ForceCreateNew))
                    {
                        var entity = _roleRepository.GetById(viewModel.Id) ?? new Core.Data.DomainModel.Role();
                        _mapper.Map(viewModel, entity);
                        _roleRepository.Update(entity);
                        scope.SaveChanges();
                    }

                    AddInfoMessage("The role has been updated/created.");

                    return RedirectToReturnUrlOrAction("RoleList");
                }
                catch (Exception ex)
                {
                    HandleException(ex, typeof(DbUpdateException));
                }
            }

            return View("RoleEdit.Generated", viewModel);
        }

        [ReadOnlyDbScope]
        public virtual ActionResult RoleClone(int id)
        {
            try
            {
                var entity = _roleRepository.Get().Where(x => x.Id == id).FirstOrDefault();
                if (entity == null)
                {
                    throw new DomainException(string.Format("The role with ID = '{0}' not found.", id));
                }
                var viewModel = _mapper.Map<RoleViewModel>(entity);
                viewModel.Id = 0;
                return RoleEdit(null, viewModel);

            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }
            return RedirectToAction("RoleList");
        }


        public virtual ActionResult RoleList()
        {
            return View("RoleList.Generated");
        }

        [NoCache]
        [ReadOnlyDbScope]
        public virtual ActionResult _RoleList()
        {
            var query = _roleRepository.Get().Select(x => new RoleListItemViewModel
            {
                Name = x.Name,
                Id = x.Id
            });
            ViewBag.ItemsPerPage = _configService.ItemsPerPage;
            return PartialView("_RoleList.Generated", query);
        }

        [NoCache]
        public virtual ActionResult RoleDelete(int id)
        {
            try
            {
                using (var scope = _dbContextScopeFactory.Create())
                {
                    var entity = _roleRepository.GetById(id);

                    if (entity == null)
                    {
                        throw new DomainException(string.Format("The role with ID = '{0}' not found.", id));
                    }
                    _roleRepository.Delete(entity);

                    scope.SaveChanges();
                }

                AddInfoMessage("The role has been deleted.");
            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }

            return RedirectToReturnUrlOrAction("RoleList");
        }

        public virtual ActionResult UserImport()
        {
            return View("UserImport.Generated");
        }

        [HttpPost]
        public virtual ActionResult UserImport(HttpPostedFileBase file)
        {
            if (file?.InputStream != null)
                try
                {
                    var entities = ContentUtilsService.GetEntities<User>(file.InputStream, file.FileName);
                    using (var scope = _dbContextScopeFactory.Create())
                    {
                        _userRepository.Import(entities);
                        scope.SaveChanges();
                    }
                    AddInfoMessage("The User data has been imported.");
                    return RedirectToAction("UserList");
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }

            return View("UserImport.Generated");
        }

        [NoCache]
        public virtual ActionResult UserExport()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _userRepository.Get().ToList();

                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };

                var json = JsonConvert.SerializeObject(entities, Formatting.Indented, settings);

                return File(System.Text.Encoding.UTF8.GetBytes(json), "application/json", "UserExport.json");
            }
        }

        [NoCache]
        public virtual ActionResult UserExportToCsv()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _userRepository.Get().ToList();

                using (var stream = new MemoryStream())
                using (var reader = new StreamReader(stream))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(entities);
                    writer.Flush();
                    stream.Position = 0;
                    var text = reader.ReadToEnd();
                    return File(System.Text.Encoding.UTF8.GetBytes(text), "text/csv", "UserExport.csv");
                }
            }
        }

        [ReadOnlyDbScope]
        public virtual ActionResult UserEdit(int? id, UserViewModel model)
        {

            ModelState.Clear();

            if (!id.HasValue)
            {
                return View("UserEdit.Generated", model);
            }
            var entity = _userRepository.GetById(id.Value);

            if (entity == null)
            {
                throw new DomainException(string.Format("The user with ID = '{0}' not found.", id));
            }
            _mapper.Map(entity, model);
            return View("UserEdit.Generated", model);
        }

        [HttpPost]
        public virtual ActionResult UserEdit(UserViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var scope = _dbContextScopeFactory.Create(DbContextScopeOption.ForceCreateNew))
                    {
                        var entity = _userRepository.GetById(viewModel.Id) ?? new Core.Data.DomainModel.User();
                        _mapper.Map(viewModel, entity);
                        _userRepository.Update(entity);
                        scope.SaveChanges();
                    }

                    AddInfoMessage("The user has been updated/created.");

                    return RedirectToReturnUrlOrAction("UserList");
                }
                catch (Exception ex)
                {
                    HandleException(ex, typeof(DbUpdateException));
                }
            }

            return View("UserEdit.Generated", viewModel);
        }

        [ReadOnlyDbScope]
        public virtual ActionResult UserClone(int id)
        {
            try
            {
                var entity = _userRepository.Get().Where(x => x.Id == id).FirstOrDefault();
                if (entity == null)
                {
                    throw new DomainException(string.Format("The user with ID = '{0}' not found.", id));
                }
                var viewModel = _mapper.Map<UserViewModel>(entity);
                viewModel.Id = 0;
                return UserEdit(null, viewModel);

            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }
            return RedirectToAction("UserList");
        }


        public virtual ActionResult UserList()
        {
            return View("UserList.Generated");
        }

        [NoCache]
        [ReadOnlyDbScope]
        public virtual ActionResult _UserList()
        {
            var query = _userRepository.Get().Select(x => new UserListItemViewModel
            {
                UserName = x.UserName,
                Email = x.Email,
                PhoneNumber = x.PhoneNumber,
                EmailConfirmed = x.EmailConfirmed,
                PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                TwoFactorEnabled = x.TwoFactorEnabled,
                LockoutEnabled = x.LockoutEnabled,
                Id = x.Id
            });
            ViewBag.ItemsPerPage = _configService.ItemsPerPage;
            return PartialView("_UserList.Generated", query);
        }

        [NoCache]
        public virtual ActionResult UserDelete(int id)
        {
            try
            {
                using (var scope = _dbContextScopeFactory.Create())
                {
                    var entity = _userRepository.GetById(id);

                    if (entity == null)
                    {
                        throw new DomainException(string.Format("The user with ID = '{0}' not found.", id));
                    }
                    _userRepository.Delete(entity);

                    scope.SaveChanges();
                }

                AddInfoMessage("The user has been deleted.");
            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }

            return RedirectToReturnUrlOrAction("UserList");
        }

        public virtual ActionResult EmployeeImport()
        {
            return View("EmployeeImport.Generated");
        }

        [HttpPost]
        public virtual ActionResult EmployeeImport(HttpPostedFileBase file)
        {
            if (file?.InputStream != null)
                try
                {
                    var entities = ContentUtilsService.GetEntities<Employee>(file.InputStream, file.FileName);
                    using (var scope = _dbContextScopeFactory.Create())
                    {
                        _employeeRepository.Import(entities);
                        scope.SaveChanges();
                    }
                    AddInfoMessage("The Employee data has been imported.");
                    return RedirectToAction("EmployeeList");
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }

            return View("EmployeeImport.Generated");
        }

        [NoCache]
        public virtual ActionResult EmployeeExport()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _employeeRepository.Get().ToList();

                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };

                var json = JsonConvert.SerializeObject(entities, Formatting.Indented, settings);

                return File(System.Text.Encoding.UTF8.GetBytes(json), "application/json", "EmployeeExport.json");
            }
        }

        [NoCache]
        public virtual ActionResult EmployeeExportToCsv()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _employeeRepository.Get().ToList();

                using (var stream = new MemoryStream())
                using (var reader = new StreamReader(stream))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(entities);
                    writer.Flush();
                    stream.Position = 0;
                    var text = reader.ReadToEnd();
                    return File(System.Text.Encoding.UTF8.GetBytes(text), "text/csv", "EmployeeExport.csv");
                }
            }
        }

        [ReadOnlyDbScope]
        public virtual ActionResult EmployeeEdit(int? id, EmployeeViewModel model)
        {
            ViewBag.Projects = _projectRepository.Get().Select(x => new ProjectListItemViewModel
            {
                Number = x.Number,
                Id = x.Id
            });

            ModelState.Clear();

            if (!id.HasValue)
            {
                return View("EmployeeEdit.Generated", model);
            }
            var entity = _employeeRepository.GetById(id.Value);

            if (entity == null)
            {
                throw new DomainException(string.Format("The employee with ID = '{0}' not found.", id));
            }
            _mapper.Map(entity, model);
            model.ProjectIds = entity.Projects.Select(entityProject => entityProject.Id).ToList();
            return View("EmployeeEdit.Generated", model);
        }

        [HttpPost]
        [ReadOnlyDbScope]
        public virtual ActionResult EmployeeEdit(EmployeeViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var scope = _dbContextScopeFactory.Create(DbContextScopeOption.ForceCreateNew))
                    {
                        var entity = _employeeRepository.GetById(viewModel.Id) ?? new Core.Data.DomainModel.Employee();
                        _mapper.Map(viewModel, entity);

                        if (viewModel.ProjectIds != null)
                        {
                            if (entity.Projects == null)
                            {
                                entity.Projects = new List<Project>();
                            }
                            entity.Projects.Clear();
                            if (viewModel.ProjectIds != null)
                            {
                                foreach (var selectedProject in viewModel.ProjectIds.Select(selectedProjectId => _projectRepository.GetById(selectedProjectId)))
                                {
                                    if (selectedProject != null)
                                    {
                                        entity.Projects.Add(selectedProject);
                                    }
                                }
                            }
                        }
                        _employeeRepository.Update(entity);
                        scope.SaveChanges();
                    }

                    AddInfoMessage("The employee has been updated/created.");

                    return RedirectToReturnUrlOrAction("EmployeeList");
                }
                catch (Exception ex)
                {
                    HandleException(ex, typeof(DbUpdateException));
                }
            }

            ViewBag.Projects = _projectRepository.Get()
                .Select(x => new ProjectListItemViewModel
                {
                    Number = x.Number,
                    Id = x.Id
                });
            return View("EmployeeEdit.Generated", viewModel);
        }

        [ReadOnlyDbScope]
        public virtual ActionResult EmployeeClone(int id)
        {
            try
            {
                var entity = _employeeRepository.Get().Where(x => x.Id == id).FirstOrDefault();
                if (entity == null)
                {
                    throw new DomainException(string.Format("The employee with ID = '{0}' not found.", id));
                }
                var viewModel = _mapper.Map<EmployeeViewModel>(entity);
                viewModel.Id = 0;
                return EmployeeEdit(null, viewModel);

            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }
            return RedirectToAction("EmployeeList");
        }


        public virtual ActionResult EmployeeList()
        {
            return View("EmployeeList.Generated");
        }

        [NoCache]
        [ReadOnlyDbScope]
        public virtual ActionResult _EmployeeList()
        {
            var query = _employeeRepository.Get().Select(x => new EmployeeListItemViewModel
            {
                Name = x.Name,
                Id = x.Id
            });
            ViewBag.ItemsPerPage = _configService.ItemsPerPage;
            return PartialView("_EmployeeList.Generated", query);
        }

        [NoCache]
        public virtual ActionResult EmployeeDelete(int id)
        {
            try
            {
                using (var scope = _dbContextScopeFactory.Create())
                {
                    var entity = _employeeRepository.GetById(id);

                    if (entity == null)
                    {
                        throw new DomainException(string.Format("The employee with ID = '{0}' not found.", id));
                    }
                    _employeeRepository.Delete(entity);

                    scope.SaveChanges();
                }

                AddInfoMessage("The employee has been deleted.");
            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }

            return RedirectToReturnUrlOrAction("EmployeeList");
        }

        public virtual ActionResult ProjectImport()
        {
            return View("ProjectImport.Generated");
        }

        /*   [HttpPost]
           public virtual ActionResult ProjectImport(HttpPostedFileBase file)
           {
               if (file?.InputStream != null)
                   try
                   {
                       var entities = ContentUtilsService.GetEntities<Project>(file.InputStream, file.FileName);
                       using (var scope = _dbContextScopeFactory.Create())
                       {
                           using (var context = scope.DbContexts.Get<MainDbContext>())
                           {
                               var detachedEmployeesIds = new List<int>();
                               foreach (var entity in entities)
                               {
                                   foreach (var employee in entity.Employees)
                                   {
                                       employee.Id =
                                           context.Employees.Where(x => x.Identity == employee.Identity)
                                               .Select(y => y.Id)
                                               .FirstOrDefault();
                                       if (employee.Id == 0)
                                       {
                                           context.Employees.Add(employee);
                                       }
                                       else
                                       {
                                           var attachedEmployeesIds =
                                               context.ChangeTracker.Entries<Employee>().Select(x => x.Entity.Id).ToList();
                                           if (!attachedEmployeesIds.Contains(employee.Id))
                                           {
                                               context.Employees.Attach(employee);
                                               context.Entry(employee).State = EntityState.Modified;
                                           }
                                           else
                                           {
                                               detachedEmployeesIds.Add(employee.Id);
                                           }
                                       }
                                   }
                                   foreach (var id in detachedEmployeesIds)
                                   {
                                       var detachedEmployee = entity.Employees.Where(x => x.Id == id).FirstOrDefault();
                                       entity.Employees.Remove(detachedEmployee);
                                       entity.Employees.Add(context.ChangeTracker.Entries<Employee>().
                                                   Where(x => x.Entity.Identity == detachedEmployee.Identity).
                                                   Select(y => y.Entity).
                                                   FirstOrDefault());
                                   }
                               }
                               _projectRepository.Import(entities);
                               scope.SaveChanges();
                           }
                       }
                       AddInfoMessage("The Project data has been imported.");
                       return RedirectToAction("ProjectList");
                   }
                   catch (Exception ex)
                   {
                       HandleException(ex);
                   }
               return View("ProjectImport.Generated");
           }*/

        [HttpPost]
        public virtual ActionResult ProjectImport(HttpPostedFileBase file)
        {
            if (file?.InputStream != null)
                try
                {
                    var projectsToImport = ContentUtilsService.GetEntities<Project>(file.InputStream, file.FileName);
                    using (var scope = _dbContextScopeFactory.Create())
                    {
                        using (var context = scope.DbContexts.Get<MainDbContext>())
                        {
                            var existingProjects = context.Projects.Include(x => x.Employees).ToList();
                            if (existingProjects == null) existingProjects = new List<Project>();
                            var existingEmployees = existingProjects.SelectMany(x => x.Employees).Distinct().ToList();
                            var addedEmployees = new List<Employee>();
                            foreach (var project in projectsToImport)
                            {
                                var theProject = existingProjects.Where(x => x.Identity == project.Identity).FirstOrDefault();
                                if (theProject == null)
                                {
                                    theProject = new Project();
                                    theProject.Employees = new List<Employee>();
                                    existingProjects.Add(theProject);
                                }
                                var propInfo = project.GetType().GetProperties().Where(x => x.Name != "Id" && x.Name != "Employees");
                                foreach (var item in propInfo)
                                {
                                    theProject.GetType().GetProperty(item.Name).SetValue(theProject, item.GetValue(project, null), null);
                                }
                                var theEmployee = new Employee();
                                foreach (var employee in project.Employees)
                                {
                                    theEmployee =
                                        existingEmployees.Where(x => x.Identity == employee.Identity).FirstOrDefault();
                                    if (theEmployee == null)
                                    {
                                        theEmployee =
                                            addedEmployees.Where(x => x.Identity == employee.Identity).FirstOrDefault();
                                        if (theEmployee == null) theEmployee = new Employee();
                                        theProject.Employees.Add(theEmployee);
                                        addedEmployees.Add(theEmployee);
                                    }
                                    var propEmpInfo =
                                        employee.GetType()
                                            .GetProperties()
                                            .Where(x => x.Name != "Id" && x.Name != "Projects");
                                    foreach (var item in propEmpInfo)
                                    {
                                        theEmployee.GetType()
                                            .GetProperty(item.Name)
                                            .SetValue(theEmployee, item.GetValue(employee, null), null);
                                    }
                                }
                                if (!context.Projects.Select(x => x.Identity).ToList().Contains(theProject.Identity))
                                context.Projects.Add(theProject);
                            }
                            scope.SaveChanges();
                        }
                    }
                    AddInfoMessage("The Project data has been imported.");
                    return RedirectToAction("ProjectList");
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            return View("ProjectImport.Generated");
        }


        [NoCache]
        public virtual ActionResult ProjectExport()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _projectRepository.Get().ToList();

                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };

                //var json = JsonConvert.SerializeObject(entities, Formatting.Indented, settings);
                var json = CustomSerializer.SerializeObject(entities, 2);
                return File(System.Text.Encoding.UTF8.GetBytes(json), "application/json", "ProjectExport.json");
            }
        }

        [NoCache]
        public virtual ActionResult ProjectExportToCsv()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _projectRepository.Get().ToList();

                using (var stream = new MemoryStream())
                using (var reader = new StreamReader(stream))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(entities);
                    writer.Flush();
                    stream.Position = 0;
                    var text = reader.ReadToEnd();
                    return File(System.Text.Encoding.UTF8.GetBytes(text), "text/csv", "ProjectExport.csv");
                }
            }
        }

        [ReadOnlyDbScope]
        public virtual ActionResult ProjectEdit(int? id, ProjectViewModel model)
        {
            ViewBag.Employees = _employeeRepository.Get().Select(x => new EmployeeListItemViewModel
            {
                Name = x.Name,
                Id = x.Id
            });

            ModelState.Clear();

            if (!id.HasValue)
            {
                return View("ProjectEdit.Generated", model);
            }
            var entity = _projectRepository.GetById(id.Value);

            if (entity == null)
            {
                throw new DomainException(string.Format("The project with ID = '{0}' not found.", id));
            }
            _mapper.Map(entity, model);
            model.EmployeeIds = entity.Employees.Select(entityEmployee => entityEmployee.Id).ToList();
            return View("ProjectEdit.Generated", model);
        }

        [HttpPost]
        [ReadOnlyDbScope]
        public virtual ActionResult ProjectEdit(ProjectViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var scope = _dbContextScopeFactory.Create(DbContextScopeOption.ForceCreateNew))
                    {
                        var entity = _projectRepository.GetById(viewModel.Id) ?? new Core.Data.DomainModel.Project();
                        _mapper.Map(viewModel, entity);

                        if (viewModel.EmployeeIds != null)
                        {
                            if (entity.Employees == null)
                            {
                                entity.Employees = new List<Employee>();
                            }
                            entity.Employees.Clear();
                            if (viewModel.EmployeeIds != null)
                            {
                                foreach (var selectedEmployee in viewModel.EmployeeIds.Select(selectedEmployeeId => _employeeRepository.GetById(selectedEmployeeId)))
                                {
                                    if (selectedEmployee != null)
                                    {
                                        entity.Employees.Add(selectedEmployee);
                                    }
                                }
                            }
                        }
                        _projectRepository.Update(entity);
                        scope.SaveChanges();
                    }

                    AddInfoMessage("The project has been updated/created.");

                    return RedirectToReturnUrlOrAction("ProjectList");
                }
                catch (Exception ex)
                {
                    HandleException(ex, typeof(DbUpdateException));
                }
            }

            ViewBag.Employees = _employeeRepository.Get()
                .Select(x => new EmployeeListItemViewModel
                {
                    Name = x.Name,
                    Id = x.Id
                });
            return View("ProjectEdit.Generated", viewModel);
        }

        [ReadOnlyDbScope]
        public virtual ActionResult ProjectClone(int id)
        {
            try
            {
                var entity = _projectRepository.Get().Where(x => x.Id == id).FirstOrDefault();
                if (entity == null)
                {
                    throw new DomainException(string.Format("The project with ID = '{0}' not found.", id));
                }
                var viewModel = _mapper.Map<ProjectViewModel>(entity);
                viewModel.Id = 0;
                return ProjectEdit(null, viewModel);

            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }
            return RedirectToAction("ProjectList");
        }


        public virtual ActionResult ProjectList()
        {
            return View("ProjectList.Generated");
        }

        [NoCache]
        [ReadOnlyDbScope]
        public virtual ActionResult _ProjectList()
        {
            var query = _projectRepository.Get().Select(x => new ProjectListItemViewModel
            {
                Number = x.Number,
                Id = x.Id
            });
            ViewBag.ItemsPerPage = _configService.ItemsPerPage;
            return PartialView("_ProjectList.Generated", query);
        }

        [NoCache]
        public virtual ActionResult ProjectDelete(int id)
        {
            try
            {
                using (var scope = _dbContextScopeFactory.Create())
                {
                    var entity = _projectRepository.GetById(id);

                    if (entity == null)
                    {
                        throw new DomainException(string.Format("The project with ID = '{0}' not found.", id));
                    }
                    _projectRepository.Delete(entity);

                    scope.SaveChanges();
                }

                AddInfoMessage("The project has been deleted.");
            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }

            return RedirectToReturnUrlOrAction("ProjectList");
        }

        public virtual ActionResult ConfigImport()
        {
            return View("ConfigImport.Generated");
        }

        [HttpPost]
        public virtual ActionResult ConfigImport(HttpPostedFileBase file)
        {
            if (file?.InputStream != null)
                try
                {
                    var entities = ContentUtilsService.GetEntities<Config>(file.InputStream, file.FileName);
                    using (var scope = _dbContextScopeFactory.Create())
                    {
                        _configRepository.Import(entities);
                        scope.SaveChanges();
                    }
                    AddInfoMessage("The Config data has been imported.");
                    return RedirectToAction("ConfigList");
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }

            return View("ConfigImport.Generated");
        }

        [NoCache]
        public virtual ActionResult ConfigExport()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _configRepository.Get().ToList();

                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };

                var json = JsonConvert.SerializeObject(entities, Formatting.Indented, settings);

                return File(System.Text.Encoding.UTF8.GetBytes(json), "application/json", "ConfigExport.json");
            }
        }

        [NoCache]
        public virtual ActionResult ConfigExportToCsv()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entities = _configRepository.Get().ToList();

                using (var stream = new MemoryStream())
                using (var reader = new StreamReader(stream))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(entities);
                    writer.Flush();
                    stream.Position = 0;
                    var text = reader.ReadToEnd();
                    return File(System.Text.Encoding.UTF8.GetBytes(text), "text/csv", "ConfigExport.csv");
                }
            }
        }

        [ReadOnlyDbScope]
        public virtual ActionResult ConfigEdit(int? id, ConfigViewModel model)
        {

            ModelState.Clear();

            if (!id.HasValue)
            {
                return View("ConfigEdit.Generated", model);
            }
            var entity = _configRepository.GetById(id.Value);

            if (entity == null)
            {
                throw new DomainException(string.Format("The config with ID = '{0}' not found.", id));
            }
            _mapper.Map(entity, model);
            return View("ConfigEdit.Generated", model);
        }

        [HttpPost]
        public virtual ActionResult ConfigEdit(ConfigViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var scope = _dbContextScopeFactory.Create(DbContextScopeOption.ForceCreateNew))
                    {
                        var entity = _configRepository.GetById(viewModel.Id) ?? new Core.Data.DomainModel.Config();
                        _mapper.Map(viewModel, entity);
                        _configRepository.Update(entity);
                        scope.SaveChanges();
                    }

                    AddInfoMessage("The config has been updated/created.");

                    return RedirectToReturnUrlOrAction("ConfigList");
                }
                catch (Exception ex)
                {
                    HandleException(ex, typeof(DbUpdateException));
                }
            }

            return View("ConfigEdit.Generated", viewModel);
        }

        [ReadOnlyDbScope]
        public virtual ActionResult ConfigClone(int id)
        {
            try
            {
                var entity = _configRepository.Get().Where(x => x.Id == id).FirstOrDefault();
                if (entity == null)
                {
                    throw new DomainException(string.Format("The config with ID = '{0}' not found.", id));
                }
                var viewModel = _mapper.Map<ConfigViewModel>(entity);
                viewModel.Id = 0;
                return ConfigEdit(null, viewModel);

            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }
            return RedirectToAction("ConfigList");
        }


        public virtual ActionResult ConfigList()
        {
            return View("ConfigList.Generated");
        }

        [NoCache]
        [ReadOnlyDbScope]
        public virtual ActionResult _ConfigList()
        {
            var query = _configRepository.Get().Select(x => new ConfigListItemViewModel
            {
                Key = x.Key,
                Value = x.Value,
                Id = x.Id
            });
            ViewBag.ItemsPerPage = _configService.ItemsPerPage;
            return PartialView("_ConfigList.Generated", query);
        }

        [NoCache]
        public virtual ActionResult ConfigDelete(int id)
        {
            try
            {
                using (var scope = _dbContextScopeFactory.Create())
                {
                    var entity = _configRepository.GetById(id);

                    if (entity == null)
                    {
                        throw new DomainException(string.Format("The config with ID = '{0}' not found.", id));
                    }
                    _configRepository.Delete(entity);

                    scope.SaveChanges();
                }

                AddInfoMessage("The config has been deleted.");
            }
            catch (Exception ex)
            {
                HandleException(ex, typeof(DbUpdateException));
            }

            return RedirectToReturnUrlOrAction("ConfigList");
        }
    }
}
