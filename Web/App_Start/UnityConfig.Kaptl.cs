/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Core.Data.Repository;
using Core.Services;
using Core.Web.Common;
using Core.Web.Infrastructure;
using Core.Web.Services;
using Core.Web.UploadFilters;
using Mehdime.Entity;
using Microsoft.Practices.Unity;
using Web.Services;

namespace Web
{
    public static partial class UnityConfig
    {
        public static void RegisterKaptlServices()
        {
            Container.RegisterInstance(AutoMapperConfig.MapperConfiguration.CreateMapper());
            Container.RegisterType<IAmbientDbContextLocator, AmbientDbContextLocator>();
            Container.RegisterInstance<IDbContextScopeFactory>(new DbContextScopeFactory());
            
            Container.RegisterType<IHttpContextService, HttpContextService>();
            Container.RegisterType<IEmailService, EmailService>();            
            Container.RegisterType<IUploadService, UploadService>();
            Container.RegisterType<IMediaProcessorService, MediaProcessorService>();
            Container.RegisterType<IContentUtilsService, ContentUtilsService>();
            Container.RegisterType<IEnumerable<IUploadFilter>, IUploadFilter[]>();

            RegisterRepositories();
        }

        private static void RegisterRepositories()
        {
            foreach (var type in typeof(DomainException).Assembly.GetTypes().
                Where(type => type.IsAssignableToGenericType(typeof(GenericRepository<>))))
            {
                var itype = type.GetInterfaces().Last();
                Container.RegisterType(itype, type);
            }
        }

    }
}
