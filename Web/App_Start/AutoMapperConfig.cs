﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Core.Web.Infrastructure.AutoMapper;

namespace Web
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration MapperConfiguration { get; private set; }

        public static void Configure(IEnumerable<Assembly> argAssemblies = null)
        {
            var assemblies = argAssemblies?.ToArray() ?? AppDomain.CurrentDomain.GetAssemblies();

            MapperConfiguration = new MapperConfiguration(configuration => ApplyConfiguration(configuration, assemblies));
        }

        private static void ApplyConfiguration(IMapperConfigurationExpression configuration, IEnumerable<Assembly> assemblies)
        {
            foreach (var profileClass in assemblies.Select(assembly => assembly.GetTypes()
                .Where(type => type != typeof(Profile) && typeof(Profile).IsAssignableFrom(type) && !type.IsAbstract)
                .ToArray()).SelectMany(profileClasses => profileClasses))
                configuration.AddProfile((Profile)Activator.CreateInstance(profileClass));

            RegisterMappings(configuration);
        }

        private static void RegisterMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<DateTime?, DateTime>().ConvertUsing<NullableDateTimeToDateTimeConverter>();
            configuration.CreateMap<DateTime?, DateTime?>().ConvertUsing<NullableToNullableDateTimeConverter>();
            configuration.CreateMap<DateTime, DateTime?>().ConvertUsing<DateTimeToNullableDateTimeConverter>();
        }
    }
}
