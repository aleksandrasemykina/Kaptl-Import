﻿/*
The MIT License (MIT)

Copyright (C) 2016, 8th Sphere Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web;
using Core.Web.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Web.Identity;
using Web.Models.Account;
using Web.Services;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        private readonly IAuthenticationManager authenticationManager;
        private readonly IHttpContextService httpContextService;
        private readonly ApplicationSignInManager signInManager;
        private readonly ApplicationUserManager userManager;

        public AccountController(IHttpContextService httpContextService,
            ApplicationUserManager userManager,
            ApplicationSignInManager signInManager,
            IAuthenticationManager authenticationManager)
        {
            this.httpContextService = httpContextService;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.authenticationManager = authenticationManager;
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult Login(string returnUrl)
        {
            var model = new LoginViewModel
            {
                UserName = httpContextService.GetCookie("LastLogin"),
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var result =
                    await signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, true);
                switch (result)
                {
                    case SignInStatus.Success:
                        httpContextService.SetCookie("LastLogin", model.UserName, true);
                        return RedirectToLocal(model.ReturnUrl);

                    case SignInStatus.LockedOut:
                        throw new UserErrorException("This account has been locked out, please try again later.");

                    //case SignInStatus.RequiresVerification:
                    //    return RedirectToAction("SendCode", new {model.ReturnUrl, model.RememberMe});

                    default:
                        throw new UserErrorException("The user name or password provided is incorrect.");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return View(model);
        }

        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await signInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            var user = await userManager.FindByIdAsync(await signInManager.GetVerifiedUserIdAsync());
            if (user != null)
            {
                // ReSharper disable once UnusedVariable
                var code = await userManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            }
            return View(new VerifyCodeViewModel {Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            // You can configure the account lockout settings in IdentityConfig
            try
            {
                var result =
                    await signInManager.TwoFactorSignInAsync(model.Provider, model.Code, model.RememberMe, false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(model.ReturnUrl);

                    case SignInStatus.LockedOut:
                        throw new UserErrorException("This account has been locked out, please try again later.");

                    //case SignInStatus.RequiresVerification:
                    //    return RedirectToAction("SendCode", new {model.ReturnUrl, model.RememberMe});

                    default:
                        throw new UserErrorException("Invalid code.");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return View(model);
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            //IdentityResult result = await userManager.AddPasswordAsync(User.Identity.GetUserId(), "NewPassword");
            var result =
                await userManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                AddInfoMessage("The password has been changed.");
                return RedirectToAction("LogOff", "Account");
            }
            AddErrors(result);
            return View(model);
        }

        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            if (code == null)
            {
                return View("Error");
            }
            var result = await userManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByNameAsync(model.UserName);
                if (user == null /*|| !await userManager.IsEmailConfirmedAsync(user.Id)*/)
                    return View("ForgotPasswordConfirmation");

                var code = await userManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new {userId = user.Id, code});
                await userManager.SendEmailAsync(user.Id, "Reset Password",
                    "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                return RedirectToAction("ForgotPasswordConfirmation");
            }

            return View(model);
        }

        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        public ActionResult ResetPassword(string code)
        {
            return View(new ResetPasswordViewModel {Code = code});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
                return RedirectToAction("ResetPasswordConfirmation");

            var result = await userManager.ResetPasswordAsync(user.Id, model.Code, model.NewPassword);
            if (result.Succeeded)
                return RedirectToAction("ResetPasswordConfirmation");

            AddErrors(result);
            return View();
        }

        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        public ActionResult LogOff()
        {
            authenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
                ModelState.AddModelError("", error);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Admin", new { area = "kaptl" });
        }
    }
}
