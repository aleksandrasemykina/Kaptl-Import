﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Core.DataAccess;
using Core.Data.DomainModel;
using Core.Services;
using Core.Web.Controllers;
using Core.Web.Filters;
using Core.Web.Models;
using Core.Web.Services;
using Mehdime.Entity;
using Web.Identity;

namespace Web.Controllers
{
    public partial class HomeController : BaseController
    {
        private readonly ApplicationRoleManager _roleManager;
        private readonly ApplicationUserManager _userManager;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IUploadService _uploadService;
        private readonly IMapper _mapper;

        private readonly IRoleRepository _roleRepository;
        private readonly IUserRepository _userRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IConfigRepository _configRepository;

        public HomeController(
            ApplicationRoleManager roleManager,
            ApplicationUserManager userManager,
            IDbContextScopeFactory dbContextScopeFactory,
            IUploadService uploadService,
            IRoleRepository roleRepository,
            IUserRepository userRepository,
            IEmployeeRepository employeeRepository,
            IProjectRepository projectRepository,
            IConfigRepository configRepository,
            IMapper mapper)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _dbContextScopeFactory = dbContextScopeFactory;
            _uploadService = uploadService;
            _mapper = mapper;
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _employeeRepository = employeeRepository;
            _projectRepository = projectRepository;
            _configRepository = configRepository;
        }
    

        public ActionResult EmployeeView(int id)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entity = _employeeRepository.GetById(id);

                if (entity == null)
                    throw new DomainException(string.Format("The employee with ID = '{0}' not found.", id));

                var model = _mapper.Map<EmployeeViewModel>(entity);
                _mapper.Map(entity.Projects, model.Projects);
                return View(model);
            }
        }

        public ActionResult EmployeeList()
        {
            return View();
        }

        [NoCache]
        public ActionResult _EmployeeList()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return PartialView(_employeeRepository.Get().ToList().Select(_mapper.Map<EmployeeViewModel>));
            }
        }
    

        public ActionResult ProjectView(int id)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entity = _projectRepository.GetById(id);

                if (entity == null)
                    throw new DomainException(string.Format("The project with ID = '{0}' not found.", id));

                var model = _mapper.Map<ProjectViewModel>(entity);
                _mapper.Map(entity.Employees, model.Employees);
                return View(model);
            }
        }

        public ActionResult ProjectList()
        {
            return View();
        }

        [NoCache]
        public ActionResult _ProjectList()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return PartialView(_projectRepository.Get().ToList().Select(_mapper.Map<ProjectViewModel>));
            }
        }
    

        public ActionResult ConfigView(int id)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var entity = _configRepository.GetById(id);

                if (entity == null)
                    throw new DomainException(string.Format("The config with ID = '{0}' not found.", id));

                var model = _mapper.Map<ConfigViewModel>(entity);
                return View(model);
            }
        }

        public ActionResult ConfigList()
        {
            return View();
        }

        [NoCache]
        public ActionResult _ConfigList()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return PartialView(_configRepository.Get().ToList().Select(_mapper.Map<ConfigViewModel>));
            }
        }
    }
}
